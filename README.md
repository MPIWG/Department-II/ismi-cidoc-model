# CIDOC-CRM based data model for ISMI

This repository contains
* a wiki with [documentation of the old ISMI data model and mapping to the new model](https://gitlab.gwdg.de/MPIWG/Department-II/ismi-cidoc-model/wikis/home)
    * [Wiki](https://gitlab.gwdg.de/MPIWG/Department-II/ismi-cidoc-model/wikis/home)   
* a diagram with a basic overview of the new data model
    * [Diagram.pdf](Diagram/diagram.pdf)
* X3ML mapping files created using the [X3ML toolkit](http://www.ics.forth.gr/isl/X3MLToolkit)
    * [Mappings](Mapping/)
* Python tool to convert the RDF triple output of X3ML to named-graph quads to ingest in the triple store
    * [add_named_graphs.py](Tools/openmind-tools/add_named_graphs.py)
* a Docker setup for 3M, X3ML, Blazegraph, and Python.
    * [docker-compose.yml](docker-compose.yml)

## Installation using Docker

You can run the tools and services using [Docker](https://www.docker.com) and [docker-compose](https://docs.docker.com/compose/).

Create instance data directories:
```
mkdir -p Tools/data/3m/data
mkdir -p Tools/data/3m/db
mkdir -p Tools/data/blazegraph
```

Download and start the containers:
```
docker-compose up -d
```

Stop all containers:
```
docker-compose down
```

## Services

* 3M web editor for XML to CIDOC-CRM mappings: http://localhost:8080/3M/
* eXist database backend for 3M: http://localhost:8081/
* Blazegraph triple store: http://localhost:8082/blazegraph/
* Python container for [openmind-tools](https://gitlab.gwdg.de/MPIWG/Department-II/openmind-tools)
* Java container for [X3ML](https://github.com/isl/x3ml)

## Run data conversion scripts

0. Prepare 3M mappings for X3ML
  ```
  Tools/scripts/convert_mappings.sh
  ``` 
1. Copy ISMI XML dump file to `Data/xml/openmind-data.xml`
2. Convert to RDF triples 
  ```
  Tools/scripts/xml2nt.sh
  ```
3. Convert triples to quadruples 
  ```
  Tools/scripts/nt2nq.sh
  ```
4. Load quadruples into Blazegraph 
  ```
  Tools/scripts/nq2blazegraph.sh
  ```
5. Postprocess RDF in Blazegraph
  ```
  Tools/scripts/process_blazegraph.sh
  ```
6. Convert RDF in Blazegraph to networkX graph 
  ```
  Tools/scripts/blazegraph2nx.sh
  ```
7. Convert ISMI XML dump to networkX graph 
  ```
  Tools/scripts/xml2nx.sh
  ```
8. Compare networkX graphs from RDF and ISMI XML 
  ```
  Tools/scripts/compare2nx.sh
  ```

## Python tools

* `add_named_graphs.py`
    * use: `python3 add_named_graphs.py file1.nt file2.nt [...] outfile.nq`
    * reads triples in file1.nt... and adds named graphs based on subject uris following the ISMI uri standard (e.g. `.../ismi/text/12345/...` -> `.../ismi/text/12345/container/context`)
    * writes n-quads file with named graphs
* `postprocess_graph.py`
    * use: `python3 postproces_graph.py http://localhost:8082/blazegraph/namespace/kb/sparql`
    * fixes problems in the datamodel left by the mapping process (e.g. witness title_written_as values)
