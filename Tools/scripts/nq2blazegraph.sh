#!/bin/bash
set -e

if [ -f .env ]
then
    source .env
fi
# blazegraph port on host system
BLAZEGRAPH_PORT=${BLAZEGRAPH_PORT:-8082}

echo "purge blazegraph..."
docker compose stop blazegraph
rm Tools/data/blazegraph/blazegraph.jnl
docker compose start blazegraph

while ! curl -fs "http://localhost:${BLAZEGRAPH_PORT}/blazegraph/namespace/kb/sparql" -o /dev/null
do
  echo "waiting for blazegraph..."
  sleep 2
done

echo "loading NQ quads into blazegraph (localhost:$BLAZEGRAPH_PORT)..."
curl "http://localhost:${BLAZEGRAPH_PORT}/blazegraph/namespace/kb/sparql" -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' --data 'uri=file%3A%2F%2F%2Fdata%2Fnq%2Fismi-graphs.nq'
