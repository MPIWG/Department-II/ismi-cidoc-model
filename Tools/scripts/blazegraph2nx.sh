#!/bin/bash

echo "convert blazegraph RDF into networkx graph..."
exec docker compose run --rm tools graph/ismirdf2model.py /data/nx-graph/ismi-graph.gpickle "http://blazegraph:8080/blazegraph/namespace/kb/sparql"
