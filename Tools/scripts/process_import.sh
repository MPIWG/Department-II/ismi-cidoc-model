#!/bin/bash

echo "postprocess imported blazegraph RDF..."
exec docker compose run --rm tools graph/import/process_import_graph.py "http://blazegraph:8080/blazegraph/namespace/kb/sparql" $*
