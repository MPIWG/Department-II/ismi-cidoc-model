#!/bin/bash

echo "converting ISMI XML to CIDOC-CRM RDF in NT format..."
exec docker compose run --rm x3ml /scripts/performAllMappings.sh /data/xml /data/nt /mapping/x3ml /mapping/generator/generator.xml
