#!/bin/bash

echo "convert RDF NT triples into NQ quads..."
exec docker compose run --rm tools graph/import/add_named_graphs.py /data/nt /data/nq/ismi-graphs.nq $*
