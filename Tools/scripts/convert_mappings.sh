#!/bin/bash

# remove admin and source_info/namespaces tags from X3ML mapping files from 3M
# runs script inside Python tools container
docker compose run --rm --entrypoint=/bin/bash tools -c '
ls /mapping/Mapping*.xml | while read f
    do python3 graph/import/clean_3m_mappings.py $f /mapping/x3ml/$(basename $f)
done
'
