#!/bin/bash

echo "convert ISMI XML into networkx graph..."
exec docker compose run --rm openmind-tools import/ismixml2model.py /data/xml/openmind-data.xml /data/nx-graph/ismi-xml-graph.gpickle $*
