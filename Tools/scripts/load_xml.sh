#!/bin/bash

SCRIPTDIR=$( dirname $0 )

echo "Loading new XML data into Triplestore..."

$SCRIPTDIR/xml2nt.sh && \
$SCRIPTDIR/nt2nq.sh && \
$SCRIPTDIR/nq2blazegraph.sh && \
$SCRIPTDIR/process_blazegraph.sh
