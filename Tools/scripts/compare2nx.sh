#!/bin/bash

echo "compare two networkx graphs..."
docker compose run --rm tools graph/compare_models.py /data/nx-graph/ismi-xml-graph.gpickle /data/nx-graph/ismi-graph.gpickle $*
