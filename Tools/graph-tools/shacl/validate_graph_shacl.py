from rdflib import Graph, ConjunctiveGraph
from rdflib.namespace import Namespace
from pyshacl import validate

import logging
import argparse

__version__ = '0.1'

##
## namespaces
##
rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
crmNs = Namespace('http://www.cidoc-crm.org/cidoc-crm/')
digNs = Namespace('http://www.ics.forth.gr/isl/CRMdig/')
frbrNs = Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/')
skosNs = Namespace('http://www.w3.org/2004/02/skos/core#')
ismiNs = Namespace('http://ontologies.mpiwg-berlin.mpg.de/ismi/')
nsPrefixes = {'crm': crmNs, 'dig': digNs, 'frbroo': frbrNs, 'ismi': ismiNs}

## 
## main
##
argp = argparse.ArgumentParser(description='Validates ISMI-CIDOC RDF in triple store using SHACL rules.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('sparql_url', nargs='?', help='triplestore SPARQL endpoint URL',
                  default='http://localhost:8082/blazegraph/namespace/kb/sparql')
argp.add_argument('shape_file', nargs='?', help='SHACL RDF shape file',
                  default='ismi-shapes.ttl')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

logging.basicConfig(level=args.loglevel)

# triplestore address
sparql_endpoint = args.sparql_url

## connect to triplestore
store = ConjunctiveGraph('SPARQLUpdateStore')
logging.info(f"connecting to SPARQL endpoint {sparql_endpoint}")
store.open((sparql_endpoint, sparql_endpoint))
for pref in nsPrefixes:
    store.bind(pref, nsPrefixes[pref])

## read shapes
shape_graph = Graph()
logging.info(f"reading SHACL shape file {args.shape_file}")
with open(args.shape_file, 'r') as f:
    shape_graph.parse(f)

# validate
logging.info("validating...")
conforms, results_graph, results_text = validate(store,
      shacl_graph=shape_graph,
      ont_graph=None,
      inference='none',
      abort_on_first=True,
      allow_infos=False,
      allow_warnings=False,
      meta_shacl=False,
      advanced=False,
      js=False,
      debug=False)

logging.info(f"validation results {conforms=}, {results_graph}, {results_text=}")
