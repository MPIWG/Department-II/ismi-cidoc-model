from rdflib import Graph, Literal, URIRef, ConjunctiveGraph
from rdflib.namespace import Namespace
from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore
from bs4 import BeautifulSoup, Comment
import logging
import requests
import re
import sys
import argparse

__version__ = '1.6'

##
## namespaces
##
rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
crmNs = Namespace('http://www.cidoc-crm.org/cidoc-crm/')
digNs = Namespace('http://www.ics.forth.gr/isl/CRMdig/')
frbrNs = Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/')
skosNs = Namespace('http://www.w3.org/2004/02/skos/core#')
ismiNs = Namespace('http://ontologies.mpiwg-berlin.mpg.de/ismi/')
ismiIdTypeNs = Namespace('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/identifier/')
nsPrefixes = {'crm': crmNs, 'dig': digNs, 'frbroo': frbrNs, 'ismi': ismiNs, 'ismiidtype': ismiIdTypeNs}

##
## patterns
##
ismi_object_id_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/([-\w]+)/([-\w]+)')
ismi_type_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/type/([-\w]+)/([-\w]+)')
ismi_bibliography_pattern = re.compile(r'http://ismi.mpiwg-berlin.mpg.de/bibliography/(\d+)')

##
## URLs
##
ismi_biblio_list_api_url = 'https://ismi.mpiwg-berlin.mpg.de/api/biblio-info-all'
ismi_private_manifests_api_url = 'https://ismi-images.medeniyet.edu.tr/iiif/manifests-internal/'
ismi_content_base_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi'
last_ismi_id_uri = ismi_content_base_uri + '/db/last-ismi-id'
last_ismi_graph_uri = ismi_content_base_uri + '/db/container/context'
external_graph_uri = ismi_content_base_uri + '/external/container/context'
ismi_group_uri = ismi_content_base_uri + '/%s'
ismi_group_graph_uri = ismi_group_uri + '/container/context'
bibliography_base_uri = 'http://ismi.mpiwg-berlin.mpg.de/bibliography'
bibliography_graph_uri = ismi_content_base_uri + '/bibliography/container/context'
ismi_bibentry_type = ismi_content_base_uri + '/type/uri/ismi-bibliography-entry'
digitization_uri = ismi_content_base_uri + '/digitization/%s'
digitization_graph_uri= digitization_uri + '/container/context'
private_manifest_uri = 'https://ismi-images.medeniyet.edu.tr/iiif/manifests/%s'

# types that should keep their ismi_id
preferred_id_types = ['person', 'text', 'witness', 'codex', 'collection', 'repository']


def update_ismi_bibliography_labels(store):
    """
    Update all ISMI bibliography URIs from the ismi server.
    """
    biblio_info = []
    # fetch all biblio_ref from ismi server
    page = 0
    logging.info(f"loading ismi bibliography...")
    try:
        for page in range(0, 100):
            with requests.Session() as http_session:
                http_response = http_session.get(ismi_biblio_list_api_url, params={'items_per_page': 100, 'page': page})
                http_response.raise_for_status()
                biblio_data = http_response.json()
                if not biblio_data:
                    break
                else:
                    biblio_info.extend(biblio_data)
                    logging.info(f"  loading {(page + 1) * 100}...")

    except Exception as e:
        logging.error(f"Error loading {ismi_biblio_list_api_url}! %s", e)
        return
        
    if len(biblio_info) == 0 or page >= 100:
        logging.error(f"Unable to load bibliography data!")
        return
    
    # remove all triples from bibliography graph
    graph = store.get_context(bibliography_graph_uri)
    store.remove_context(graph)
    # new context for bibliography graph
    graph = store.get_context(URIRef(bibliography_graph_uri))
    new_quads = []
    bibentry_type_uri = URIRef(ismi_bibentry_type)
    
    cnt = 0
    for bib in biblio_info:
        # create E73_Information_Object entries for all biblio entries
        uri = URIRef(bibliography_base_uri + '/' + bib['biblio_id'])
        # add crm:E73 type and crm:P2_has_type
        new_quads.append((uri, rdfNs.type, crmNs.E73_Information_Object, graph))
        new_quads.append((uri, crmNs.P2_has_type, bibentry_type_uri, graph))
        # add reference as label
        ref = f"{bib['citation_text'].strip()} {{#{bib['biblio_id']}}}"
        logging.debug(f"reference label={repr(ref)}")
        new_quads.append((uri, rdfsNs.label, Literal(ref), graph))
        cnt += 1

        # batch add quads
        if len(new_quads) > 100:
            logging.info(f"  adding {cnt}...")
            store.addN(new_quads)
            new_quads = []
            
    # add last batch
    if new_quads:
        logging.info(f"  adding {cnt}...")
        store.addN(new_quads)
    
    logging.info(f"updated {cnt} bibliography URLs.")


def _find_highest_ismi_id(store):
    query = '''SELECT (max(?nid) as ?max_id)
        WHERE {
          ?id_uri a crm:E42_Identifier ;
            crm:P2_has_type ismiidtype:ismi-id ;
            rdfs:label ?id_label .
          bind(xsd:integer(?id_label) as ?nid)
        }'''
    res = store.query(query)
    logging.debug(f"finding highest ismi_id...")
    for r in res:
        return r.max_id
    
    return None


def _create_ismi_id(store):
    last_id_uri = URIRef(last_ismi_id_uri)
    min_val = 1000000
    last_val = min_val
    for r in store[last_id_uri:rdfsNs.label]:
        value = int(r)
        if value > last_val:
            last_val = value
    
    if last_val == min_val:
        # no value stored
        last_val = _find_highest_ismi_id(store)
        if last_val is None:
            last_val = min_val
    
    new_val = last_val + 1
    
    logging.debug(f"creating new ismi_id {new_val}")
    # remove last value (in all graphs)
    store.remove((last_id_uri, rdfsNs.label, Literal(last_val)))
    # create new value in db graph
    graph = store.get_context(URIRef(last_ismi_graph_uri))
    graph.add((last_id_uri, rdfsNs.label, Literal(new_val)))
    # return as string
    return str(new_val)


def _create_digitization(label, store):
    ismi_id = _create_ismi_id(store)
    graph = store.get_context(URIRef(digitization_graph_uri%ismi_id))
    uri = URIRef(digitization_uri%ismi_id)
    new_quads = []
    # ismi:Digitization
    new_quads.append((uri, rdfNs.type, ismiNs.Digitization, graph))
    new_quads.append((uri, rdfNs.type, digNs.D2_Digitization_Process, graph))
    new_quads.append((uri, rdfsNs.label, Literal(label), graph))
    # E42_Identifier
    ismi_id_uri = URIRef(digitization_uri%ismi_id + '/identifier/ismi')
    new_quads.append((ismi_id_uri, rdfNs.type, crmNs.E42_Identifier, graph))
    new_quads.append((ismi_id_uri, crmNs.P2_has_type, URIRef('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/identifier/ismi-id'), graph))
    new_quads.append((ismi_id_uri, rdfsNs.label, Literal(ismi_id), graph))
    # iiif-manifest
    ext_graph = store.get_context(URIRef(external_graph_uri))
    manif_uri = URIRef(private_manifest_uri%label)
    new_quads.append((manif_uri, rdfNs.type, crmNs.E36_Visual_Item, ext_graph))
    new_quads.append((manif_uri, rdfNs.type, digNs.D1_Digital_Object, ext_graph))
    new_quads.append((manif_uri, crmNs.P2_has_type, URIRef('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/iiif/private-iiif-manifest'), ext_graph))
    new_quads.append((manif_uri, crmNs.P2_has_type, URIRef('https://iiif.io/api/presentation/#'), ext_graph))
    # link to digitization
    new_quads.append((uri, crmNs.P1_is_identified_by, ismi_id_uri, graph))
    new_quads.append((uri, crmNs.P94_has_created, manif_uri, graph))
    logging.debug(f"creating digitization {label} [{ismi_id}]")
    store.addN(new_quads)


def _delete_digitization(digi, store):
    ismi_id = digi['id']
    if not ismi_id:
        logging.error(f"Unable to delete digitization with empty id!")
        return

    graph_uri = URIRef(digitization_graph_uri%ismi_id)
    
    if digi['manifest2']:
        # remove private scan only
        logging.debug(f"removing private scan from digitization {ismi_id} with public scan!")
        graph = store.get_context(graph_uri)
        graph.remove((digi['uri'], crmNs.P94_has_created, digi['manifest']))
    else:
        # remove whole graph
        logging.debug(f"deleting digitization graph {graph_uri}")
        graph = store.get_context(graph_uri)
        store.remove_context(graph)


def update_ismi_private_digitizations(store):
    """
    Update all private ISMI digitizations from the ismi-images server.
    """
    # fetch list of all manifests from ismi-images server
    logging.info(f"loading ismi private manifest list from {ismi_private_manifests_api_url}")
    try:
        with requests.Session() as http_session:
            http_response = http_session.get(ismi_private_manifests_api_url)
            http_response.raise_for_status()
            manifest_data = http_response.json()
            if not manifest_data:
                logging.error(f"Empty private manifest list!")
                return

    except Exception as e:
        logging.error(f"Error loading {ismi_private_manifests_api_url}! %s", e)
        return
    
    logging.info(f"  found {len(manifest_data)} private manifests")
    
    # collect all private digitzations and store by id
    digitizations = {}
    query = '''SELECT ?digitization ?label ?graph ?manifest ?manifest2
    WHERE {
        GRAPH ?graph {
            ?digitization a ismi:Digitization ;
                rdfs:label ?label ;
                crm:P94_has_created ?manifest .
        }
        ?manifest crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/iiif/private-iiif-manifest> .
        OPTIONAL {
            ?digitization crm:P94_has_created ?manifest2 .
            ?manifest2 crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/iiif/public-iiif-manifest> .
        }
    }'''
    res = store.query(query)
    for r in res:
        match = ismi_object_id_pattern.match(r.digitization)
        if match:
            digi_id = match.group(2)
            label = str(r.label)
            if not label or label in digitizations:
                logging.error(f"Empty or existing digitization label: '{label}'")
                continue

            digitizations[label] = {
                'id': digi_id, 
                'uri': r.digitization, 
                'label': label, 
                'graph': r.graph,
                'manifest': r.manifest,
                'manifest2': r.manifest2
            }
            
    logging.info(f"  found {len(digitizations)} private digitizations")
    
    # check manifests and digitizations
    cnt = 0
    added_digis = 0
    deleted_digis = 0
    unchecked_digis = digitizations.copy()
    # check for missing digitizations
    logging.debug(f"checking missing digitizations...")
    for manif in manifest_data:
        label = manif['name'].replace('.json', '')
        if label in unchecked_digis:
            del unchecked_digis[label]
        else:
            logging.debug(f"missing digitization for manifest {label}")
            _create_digitization(label, store)
            added_digis += 1

        cnt += 1
        if cnt % 100 == 0:
            logging.debug(f"  {cnt} checked")

    # check for missing manifests
    logging.debug(f"checking digitizations without manifests...")
    if unchecked_digis:
        if len(unchecked_digis) > 100:
            logging.error(f"Too many digitizations to delete ({len(unchecked_digis)})!")
            return

        for digi in unchecked_digis:
            _delete_digitization(unchecked_digis[digi], store)
            deleted_digis += 1
    
    logging.info(f"added {added_digis} and deleted {deleted_digis} digitizations.")
    

def _delete_subject(subject_uri, graph_uri, store):
    logging.debug(f"removing subject {subject_uri}!")
    graph = store.get_context(graph_uri)
    graph.remove((subject_uri, None, None))

    
def clean_html_fields(store):
    """clean the HTML in some fields"""
    # collect all private digitzations and store by id
    html_fields = {}
    query = '''SELECT ?field ?label ?type ?graph
    WHERE {
        GRAPH ?graph {
            ?field a frbroo:F23_Expression_Fragment ;
                rdfs:label ?label ;
                crm:P2_has_type ?type .
        }
    }'''
    res = store.query(query)
    logging.info(f"checking {len(res)} possible HTML fields...")
    cnt = 0
    for r in res:
        field_uri = r.field
        graph_uri = r.graph
        label = str(r.label)
        if '<' not in label:
            # no HTML
            continue
        
        try:
            soup = BeautifulSoup(label, 'html.parser')
            
            # contains no text
            plaintext = soup.get_text()
            if not plaintext.strip():
                logging.debug(f"deleting html field {field_uri} with no text!")
                _delete_subject(field_uri, graph_uri, store)
                continue
                
            # html
            htmls = soup.find_all('html')
            if htmls:
                logging.warning(f"html field {field_uri} contains 'html'!")
                for tag in forms:
                    tag.unwrap()
                
            # head
            heads = soup.find_all('head')
            if heads:
                logging.warning(f"html field {field_uri} contains 'head'!")
                for tag in heads:
                    tag.decompose()
                
            # body
            bodys = soup.find_all('body')
            if bodys:
                logging.warning(f"html field {field_uri} contains 'body'!")
                for tag in bodys:
                    tag.unwrap()
                
            # script
            scripts = soup.find_all('script')
            if scripts:
                logging.warning(f"html field {field_uri} contains 'script'!")
                for tag in scripts:
                    tag.unwrap()
                
            # form
            forms = soup.find_all('form')
            if forms:
                logging.warning(f"html field {field_uri} contains 'form'!")
                for tag in forms:
                    tag.unwrap()
                
            # input
            inputs = soup.find_all('input')
            if inputs:
                logging.warning(f"html field {field_uri} contains 'input'!")
                for tag in inputs:
                    tag.unwrap()
            
            # comments
            comments = soup.find_all(string=lambda text: isinstance(text, Comment))
            if comments:
                logging.debug(f"removing comments in html field {field_uri}")
                for tag in comments:
                    tag.extract()
            
            # span
            spans = soup.find_all('span')
            if spans:
                logging.debug(f"removing 'span' in html field {field_uri}")
                for tag in spans:
                    tag.unwrap()
            
            # div
            divs = soup.find_all('div')
            if divs:
                logging.debug(f"cleaning 'div' in html field {field_uri}")
                for tag in divs:
                    new_attrs = {}
                    for attr in tag.attrs:
                        if attr == 'dir':
                            new_attrs[attr] = tag.attrs[attr].lower()
                            
                    tag.attrs.clear()
                    if new_attrs:
                        tag.attrs.update(new_attrs)

            # p
            ps = soup.find_all('p')
            if ps:
                logging.debug(f"cleaning 'p' in html field {field_uri}")
                for tag in ps:
                    new_attrs = {}
                    for attr in tag.attrs:
                        if attr == 'dir':
                            new_attrs[attr] = tag.attrs[attr].lower()
                            
                    tag.attrs.clear()
                    if new_attrs:
                        tag.attrs.update(new_attrs)
            
            # td
            tds = soup.find_all('td')
            if tds:
                logging.debug(f"cleaning 'td' in html field {field_uri}")
                for tag in tds:
                    if tag.attrs:
                        tag.attrs.clear()
            
            # write label
            soup.smooth()
            new_label = soup.prettify()
            if new_label != label:
                #logging.debug(f"changing html field to: {new_label}")
                logging.debug(f"changing html field {field_uri}")
                graph = store.get_context(graph_uri)
                graph.remove((field_uri, rdfsNs.label, r.label))
                graph.add((field_uri, rdfsNs.label, Literal(new_label)))
            
        except Exception as e:
            logging.error(f"Error parsing html field {field_uri}: %s", e)
            
        cnt += 1
        if cnt % 100 == 0:
            logging.info(f"  cleaning {cnt}")
    
    logging.info(f"cleaned {cnt} HTML fields.")
    

def update_ismi_ids(store):
    """update unset and duplicate ismi_ids to be unique"""
    ismi_ids = {}
    ids_to_update = []
    query = '''SELECT DISTINCT ?graph ?ismi_id ?id_uri
        WHERE {
          GRAPH ?graph {
            ?id_uri a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
        }'''
    res = store.query(query)
    for r in res:
        graph = r.graph
        ismi_id = r.ismi_id
        uri = r.id_uri
        if str(ismi_id).lower() == 'tbd':
            logging.debug(f"undefined ismi_id: {graph=} {uri=}")
            ids_to_update.append((graph, uri))
            continue
            
        if ismi_id in ismi_ids:
            logging.debug(f"duplicate ismi_id: {graph=} {uri=} {ismi_id=}")
            # prefer to keep person, etc. ids
            m = ismi_object_id_pattern.match(str(uri))
            if m:
                ismi_type = m.group(1)
                if ismi_type in preferred_id_types:
                    om = ismi_object_id_pattern.match(ismi_ids[ismi_id][1])
                    if om:
                        old_type = om.group(1)
                        if old_type in preferred_id_types:
                            # change new id
                            ids_to_update.append((graph, uri))
                        else:
                            # change old id
                            ids_to_update.append(ismi_ids[ismi_id])
                    else:        
                        # change old id
                        ids_to_update.append(ismi_ids[ismi_id])
                else:
                    # change new id
                    ids_to_update.append((graph, uri))
            else:
                # change new id
                ids_to_update.append((graph, uri))

        ismi_ids[ismi_id] = (graph, uri)
        
    for graph_uri, uri in ids_to_update:
        new_id = str(_create_ismi_id(store))
        logging.debug(f"changing ismi_id {graph_uri=} {uri=} to {new_id}")
        graph = store.get_context(graph_uri)
        graph.remove((uri, rdfsNs.label, None))
        graph.add((uri, rdfsNs.label, Literal(new_id)))
        
    logging.info(f"updated {len(ids_to_update)} ismi_ids out of {len(res)}.")
    
    
## 
## main
##
argp = argparse.ArgumentParser(description='Updates ISMI-CIDOC RDF in triple store with external data.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('sparql_url', nargs='?', help='triplestore SPARQL (update) endpoint URL',
                  default='http://localhost:8082/blazegraph/namespace/kb/sparql')
argp.add_argument('--update-ids-only', dest='update_ids_only', action='store_true', 
                  help='Only pdate missing ismi_ids.')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
argp.add_argument('--log-to', dest='log_to', default='STDERR', 
                  help='Log destination (STDOUT, STDERR).')
args = argp.parse_args()

if args.log_to == 'STDOUT':
    stream = sys.stdout
else:
    stream = sys.stderr
    
logging.basicConfig(level=args.loglevel, stream=stream)

# triplestore address
sparql_endpoint = args.sparql_url

## connect to triplestore
store = ConjunctiveGraph('SPARQLUpdateStore')
logging.info(f"connecting to SPARQL endpoint {sparql_endpoint}")
store.open((sparql_endpoint, sparql_endpoint))
for pref in nsPrefixes:
    store.bind(pref, nsPrefixes[pref])

logging.info("updating missing and duplicate ismi_ids...")
update_ismi_ids(store)
if args.update_ids_only:
    sys.exit()

logging.info("updating ismi bibliography labels...")
update_ismi_bibliography_labels(store)

logging.info("updating private ismi digitizations...")
update_ismi_private_digitizations(store)

logging.info("cleaning html in fields...")
clean_html_fields(store)
