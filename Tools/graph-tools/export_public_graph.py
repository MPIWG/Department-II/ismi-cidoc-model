from rdflib import Graph, Literal, URIRef, ConjunctiveGraph
from rdflib.namespace import Namespace
from rdflib.plugins.stores.sparqlstore import SPARQLStore
import logging
import re
import sys
import datetime
import argparse

__version__ = "1.8"

##
## namespaces
##
rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
crmNs = Namespace('http://www.cidoc-crm.org/cidoc-crm/')
digNs = Namespace('http://www.ics.forth.gr/isl/CRMdig/')
frbrNs = Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/')
skosNs = Namespace('http://www.w3.org/2004/02/skos/core#')
ismiNs = Namespace('http://ontologies.mpiwg-berlin.mpg.de/ismi/')
#ismitypeNs = Namespace('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/')
nsPrefixes = {'rdfs': rdfsNs, 'crm': crmNs, 'dig': digNs, 'frbr': frbrNs, 'skos': skosNs, 
              'ismi': ismiNs}

##
## patterns
##
ismi_object_id_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/([-\w]+)/([-\w]+)')

##
## URLs
##
ismi_group_graph_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/%s/container/context'

# types of entities that can be (non)public
# entity-types: 'person', 'text', 'witness', 'codex', 'collection', 'repository', 'place', 'reference', 'digitization', 
# 'subject', 'role', 'floruit_date', 'alias', 'transfer_event', 'study_event', 'copy_event', 'misattribution', 
# 'misidentification'
publishable_types = {'person', 'text', 'witness', 'codex', 'collection', 'repository', 'place', 'subject'}

# type-types that are grouped into a separate graph
grouped_types = [
    'role',
    'language'
]

# external types that are public
external_types = [
    'external',
    'bibliography',
    'linked-place'
]
# graphs that are public
generic_graphs = [URIRef(ismi_group_graph_uri%u) for u in external_types]
# add grouped types
generic_graphs += [URIRef(ismi_group_graph_uri%u) for u in grouped_types]

# prefixes of external URIs that are non-public
external_exclude_prefixes = ['https://ismi-images.medeniyet.edu.tr/']

logger = logging.getLogger('main')

def find_subgraphs(graph_uri, graphs):
    """
    Return a list of all named graph uris that start with graph_uri.
    """
    subgraphs = []
    query = '''SELECT distinct ?graph
    WHERE { 
      GRAPH ?graph {
        ?s rdf:type ?o .
      }
      filter(strstarts(str(?graph), "%s")) 
    }'''%graph_uri
    res = graphs.query(query)
    for r in res:
        subgraphs.append(r.graph)
        
    return subgraphs


def copy_graph(graph_uri, ingraphs, outgraphs, exclude_prefixes=None):
    """
    Copy complete named graph graph_uri from ingraphs to outgraphs
    """
    in_ctx = ingraphs.get_context(graph_uri)
    logger.debug(f"  copying graph {graph_uri}")
    quads = []
    for s, p, o in in_ctx:
        if exclude_prefixes:
            for ep in exclude_prefixes:
                if str(s).startswith(ep) or str(o).startswith(ep):
                    continue
                
        quads.append((s, p, o, graph_uri))
        
    outgraphs.addN(quads)


def filter_graph(graph_uri, ingraphs, outgraphs, public_ids):
    """
    Copy triples related to public_ids in named graph graph_uri from ingraphs to outgraphs
    """
    in_ctx = ingraphs.get_context(graph_uri)
    logger.debug(f"  filtering graph {graph_uri}")
    quads = []
    for s, p, o in in_ctx:
        s_match = ismi_object_id_pattern.match(s)
        if s_match:
            s_type = s_match.group(1)
            s_id = s_match.group(2)
            if not s_id in public_ids:
                if s_type not in publishable_types:
                    # accept non-publishable types in public graph as public
                    public_ids.add(s_id)
                else:
                    logger.error(f"non-public subject {s} in public graph {graph_uri}")
                    continue
            
        o_match = ismi_object_id_pattern.match(o)
        if o_match:
            o_type = o_match.group(1)
            o_id = o_match.group(2)
            if o_type in publishable_types and o_id not in public_ids:
                logger.debug(f"non-public object {o} in public graph: {graph_uri}")
                continue

        quads.append((s, p, o, graph_uri))
        
    outgraphs.addN(quads)


def has_public_relation(graph_uri, graphs, relation, public_graphs):
    """
    Return if the graph with graph_uri has a given relation to any public_graph.
    """
    query = '''SELECT distinct ?pub_graph
    WHERE { 
      graph ?graph {
        ?s ?relation ?rel_o .
      }
      graph ?pub_graph {
        ?rel_o ?p ?o .
      }
    }'''
    logger.debug(f"  checking graph {graph_uri}")
    res = graphs.query(query, initBindings={'graph': graph_uri, 'relation': relation})
    for r in res:
        if r.pub_graph in public_graphs:
            return True
        
    return False


def has_public_scan(graph_uri, graphs):
    """
    Return if the digitization with graph_uri has a public scan uri.
    """
    query = '''SELECT ?scan_uri
    WHERE { 
      graph ?graph {
        ?s crm:P94_has_created ?scan_uri .
      }
      ?scan_uri crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/iiif/public-iiif-manifest> .
    }'''
    logger.debug(f"  checking graph {graph_uri}")
    res = graphs.query(query, initBindings={'graph': graph_uri})
    for r in res:
        if r.scan_uri:
            return True
        
    return False


def export_public(ingraphs, outgraphs):
    """
    Export all public ISMI data from ingraphs to outgraphs.
    """ 
    # 
    # copy ismi type graphs as public
    #
    type_graphs = find_subgraphs('http://content.mpiwg-berlin.mpg.de/ns/ismi/type', ingraphs)
    logger.info(f"copying {len(type_graphs)} type graphs...")
    for g in type_graphs:
        copy_graph(g, ingraphs, outgraphs)

    # 
    # copy generic graphs as public
    #
    logger.info(f"copying {len(generic_graphs)} generic graphs...")
    for g in generic_graphs:
        if g == URIRef(ismi_group_graph_uri%'external'):
            # use exclude list for external graph
            copy_graph(g, ingraphs, outgraphs, external_exclude_prefixes)
        else:
            copy_graph(g, ingraphs, outgraphs)
        
    # 
    # collect all graphs where main-subject (same URI as graph) has type is-public
    #
    logger.info(f"searching for public graphs...")
    public_graphs = set()
    public_ids = set()
    query = '''SELECT distinct ?graph
    WHERE { 
      graph ?graph {
        ?subject crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/public/is-public> .
      }
      filter(str(?graph) = concat(str(?subject),"/container/context"))
    }'''
    res = ingraphs.query(query)
    for r in res:
        g_str = str(r.graph)
        g_match = ismi_object_id_pattern.match(g_str)
        if g_match:
            g_type = g_match.group(1)
            if g_type not in publishable_types:
                logging.warning(f"public graph {g_str} type not publishable!")
                continue
            
            public_ids.add(g_match.group(2))
            
        else:
            logging.warning(f"graph {g_str} type unknown!")
            
        public_graphs.add(r.graph)
    
    logger.info(f"found {len(public_graphs)} public graphs")
    
    #
    # copy all public graphs
    #
    logger.info("filtering public graphs...")
    cnt = 0
    for g in public_graphs:
        filter_graph(g, ingraphs, outgraphs, public_ids)
        cnt += 1
        if cnt % 100 == 0:
            logger.info(f"    {cnt}...")
        
    logger.info(f"filtered {len(public_graphs)} named graphs.")
    
    #
    # add digitization graphs with relations to public graphs
    #
    logger.info(f"searching for digitization graphs...")
    digi_graphs = find_subgraphs('http://content.mpiwg-berlin.mpg.de/ns/ismi/digitization', ingraphs)
    logger.info(f"scanning {len(digi_graphs)} digitization graphs...")
    cnt = 0
    for digi_graph in digi_graphs:
        if has_public_relation(digi_graph, ingraphs, digNs.L1_digitized, public_graphs):
            if has_public_scan(digi_graph, ingraphs):
                logging.debug(f"  copying public digitization {digi_graph}")
                copy_graph(digi_graph, ingraphs, outgraphs, exclude_prefixes=external_exclude_prefixes)
                cnt += 1
                if cnt % 100 == 0:
                    logger.info(f"    {cnt}...")
    
    logger.info(f"copied {cnt} public digitization graphs.")
    

def add_ldp_triples(graphs):
    """
    Add researchspace LDP triples to all named graphs in graphs.
    """
    from rdflib.namespace import XSD
    rsFieldConNs = Namespace('http://www.researchspace.org/resource/system/')
    mpFieldConNs = Namespace('http://www.metaphacts.com/ontologies/platform#')
    platformNs = rsFieldConNs
    rsUserNs = Namespace('http://www.researchspace.org/resource/user/')
    mpUserNs = Namespace('http://www.metaphacts.com/resource/user/')
    userNs = rsUserNs
    ldpNs = Namespace('http://www.w3.org/ns/ldp#')
    rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
    xsdNs = Namespace('http://www.w3.org/2001/XMLSchema#')
    provNs = Namespace('http://www.w3.org/ns/prov#')

    # add global researchspace form container graph
    formCtx = graphs.get_context(platformNs['Container/context'])
    # add form container definition
    formCtx.add((platformNs.formContainer, rdfsNs.label, Literal("Form Container")))
    formCtx.add((platformNs.formContainer, provNs.wasAttributedTo, userNs.admin))
    timestamp = datetime.datetime.now(datetime.timezone.utc).isoformat()
    formCtx.add((platformNs.formContainer, provNs.generatedAtTime, Literal(timestamp, datatype=XSD.dateTime)))
    formCtx.add((platformNs.rootContainer, ldpNs.contains, platformNs.formContainer))
    formCtx.add((platformNs.formContainer, rdfNs.type, ldpNs.Container))
    formCtx.add((platformNs.formContainer, rdfsNs.comment, Literal("Container to store statements created by forms.")))
    formCtx.add((platformNs.formContainer, rdfNs.type, ldpNs.Resource))
    formCtx.add((platformNs.formContainer, rdfNs.type, provNs.Entity))

    # link all named graphs to form container
    for ctx in graphs.contexts():
        ctx_uri = ctx.identifier
        container_uri = URIRef(str(ctx_uri).replace('/container/context', '/container'))
        # Platform:formContainer ldpNs:contains ?container .
        graphs.add((platformNs.formContainer, ldpNs.contains, container_uri, ctx_uri))
        # ?container a ldpNs:Container, ldpNs:Resource .
        #graphs.add((container_uri, rdfNs.type, ldpNs.Container, ctx_uri))
        graphs.add((container_uri, rdfNs.type, ldpNs.Resource, ctx_uri))


## 
## main
##
argp = argparse.ArgumentParser(description='Reads ISMI-CIDOC CRM RDF from triple store and exports public graph.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('outfile', help='RDF output file (default nquads format)')
argp.add_argument('sparql_url', nargs='?', help='triplestore SPARQL endpoint URL',
                  default='http://localhost:8082/blazegraph/namespace/kb/sparql')
argp.add_argument('--format', dest='output_format', choices=['nquads', 'trig', 'nt', 'ttl', 'xml'], default='nquads',
                  help='Output file format.')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
argp.add_argument('--log-to', dest='log_to', default='STDERR', 
                  help='Log destination (STDOUT, STDERR).')
args = argp.parse_args()

if args.log_to == 'STDOUT':
    stream = sys.stdout
else:
    stream = sys.stderr
    
logging.basicConfig(level=args.loglevel, stream=stream)

# connect to triplestore
ingraphs = ConjunctiveGraph('SPARQLStore')
logger.info(f"connecting to SPARQL endpoint {args.sparql_url}")
ingraphs.open(args.sparql_url)
for pref in nsPrefixes:
    ingraphs.bind(pref, nsPrefixes[pref])

export_flat_graph = args.output_format in ['nt', 'ttl', 'xml']
if export_flat_graph:
    logger.warning(f"Export format {args.output_format} does not preserve named graphs.")
    
# named graph output 
outgraphs = ConjunctiveGraph()
for pref in nsPrefixes:
    outgraphs.bind(pref, nsPrefixes[pref])

logger.info("extracting public data...")
export_public(ingraphs, outgraphs)

if not export_flat_graph:
    logger.info("adding LDP triples...")
    add_ldp_triples(outgraphs)

logger.info(f"writing {args.outfile} (size {len(outgraphs)}) as {args.output_format}")
outgraphs.serialize(args.outfile, format=args.output_format)
