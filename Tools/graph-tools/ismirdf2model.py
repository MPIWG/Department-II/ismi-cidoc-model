from rdflib import Graph, Dataset, Literal, URIRef
from rdflib.namespace import Namespace
from rdflib.plugins.stores import sparqlstore
import networkx
import re
import sys
import logging
import argparse
import pickle
import json
import os
import shapely

__version__ = '1.14'

##
## namespaces
##
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
crmNs = Namespace('http://www.cidoc-crm.org/cidoc-crm/')
digNs = Namespace('http://www.ics.forth.gr/isl/CRMdig/')
frbrNs = Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/')
skosNs = Namespace('http://www.w3.org/2004/02/skos/core#')
ismiNs = Namespace('http://ontologies.mpiwg-berlin.mpg.de/ismi/')
ismitypeNs = Namespace('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/')
nsPrefixes = {'rdfs': rdfsNs, 'crm': crmNs, 'dig': digNs, 'frbr': frbrNs, 'skos': skosNs, 
              'ismi': ismiNs}

##
## patterns
##
ismi_object_id_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/([-\w]+)/([-\w]+)')
ismi_alias_id_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/([-\w]+)/([-\w]+)/([-\w]+)(.*)?')
ismi_public_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/type/public/([-\w]+)')

# types that do not have the ismi_id in the uri
non_ismi_id_uri_types = {
    'reference', 
    'misattribution', 
    'misidentification', 
    'transfer-event', 
    'study-event',
    'copy-event'
}
# map of entity uris to ismi_ids
uri_ismi_id_map = {}


def get_id_from_uri(uri):
    ismi_id = None
    ismi_id_match = ismi_object_id_pattern.search(uri)
    if ismi_id_match is not None:
        ismi_type = ismi_id_match.group(1)
        if ismi_type in non_ismi_id_uri_types:
            # use saved ismi_id from map
            if uri not in uri_ismi_id_map:
                logging.error(f"unable to find ismi_id for uri {uri}!")
            else:
                ismi_id = uri_ismi_id_map[uri]
            
        else:
            # ismi_id from uri
            ismi_id = ismi_id_match.group(2)
    
    return ismi_id


def parse_entities_attributes(template, store):
    """
    Reads the given template and executes the attribute functions.
    The attribute functions return the value of the attribute for all entities.
    
    Returns a dict of attribute name and list of (uri, value)
    """
    selector = template['selector']
    data = {}
    for att_name, att_tpl in template['attributes'].items():
        # process attribute template
        for key, val in att_tpl.items():
            try:
                # get template function
                fn = getattr(TemplateFunctions, key)
                # call template function with uri
                data[att_name] = fn(selector, val, store)
                
            except Exception as e:
                logging.exception(f"Error applying attribute template function {key}({val})! %s", e)
                sys.exit(1)
    
    return data
    
    
def save_entities(data, ismi_type, nx_graph):
    """
    Unpacks the attribute and uri lists in data and creates nodes in nx_graph.
    
    Uses the ismi_id attribute in data as node identifier.
    """
    entities = {}
    # unpack all attribute lists    
    for key, val in data.items():
        if not val:
            continue
        
        for uri, att in val:
            if uri not in entities:
                # create new entity
                attrs = {}
                attrs['_type'] = ismi_type
                entities[uri] = attrs
            else:
                attrs = entities[uri]
                
            if key == 'ismi_id':
                if not att:
                    raise RuntimeError("Empty ismi_id attribute ({val})!")
                attrs['ismi_id'] = str(att)
                
            else:
                attrs[key] = str(att)

    # save all entities
    logging.info(f"  adding {len(entities)} entities")
    for uri, attrs in entities.items():
        ismi_id = attrs.get('ismi_id', None)
        if ismi_id is None:
            raise RuntimeError(f"Missing ismi_id for {uri}!")
        
        if ismi_id in nx_graph:
            logging.debug(f"Node id {ismi_id} exists already!")
            old_attrs = nx_graph.nodes[ismi_id]
            if attrs != old_attrs:
                logging.error(f"Node id {ismi_id} exists with different content! (old: {nx_graph.nodes[ismi_id]} new: {attrs})")
            
        nx_graph.add_node(ismi_id, **attrs)


def parse_entities_relations(template, store):
    """
    Applies the given template and executes the relation functions.
    The relation functions return the value of the relations for all entities.
    
    Returns a dict of relation name and list of (uri, uri)
    """
    selector = template['selector']
    data = {}
    for att_name, att_tpl in template['relations'].items():
        # process attribute template
        for key, val in att_tpl.items():
            try:
                # get template function
                fn = getattr(TemplateFunctions, key)
                # call template function with uri
                data[att_name] = fn(selector, val, store)
                
            except Exception as e:
                logging.exception(f"Error applying relation template function {key}({val})! %s", e)
                sys.exit(1)
    
    return data
    
    
def save_relations(data, ismi_type, nx_graph):
    """
    Unpacks the source and target uri lists in data and creates relations in nx_graph.
    
    Uses the ismi_id attribute in data as node identifier.
    """
    cnt = 0
    # unpack all attribute lists    
    for key, val in data.items():
        if not val:
            continue
        
        for src, tar in val:
            attrs = {'_type': key}
            if not (src in nx_graph and tar in nx_graph):
                raise RuntimeError(f"Relation between unknown nodes ({src}, {key}, {tar})!")
                
            # create relation 
            nx_graph.add_edge(src, tar, attr_dict=attrs)
            cnt += 1

    # save all entities
    logging.info(f"  added {cnt} relations")


def save_graph(filename, nx_graph):
    with open(filename, 'wb') as f:
        pickle.dump(nx_graph, f, pickle.HIGHEST_PROTOCOL)
    
    
def process_all_entities(store, mappings, nx_graph):
    for entity_type in mappings:
        logging.info(f"reading attributes for {entity_type}")
        template = mappings[entity_type]
        data = parse_entities_attributes(template, store)
        logging.info(f"  {len(data)} attribute types")
        logging.info(f"adding entities for {entity_type}")
        save_entities(data, entity_type, nx_graph)
        
    for entity_type in mappings:
        logging.info(f"reading relations for {entity_type}")
        template = mappings[entity_type]
        data = parse_entities_relations(template, store)
        logging.info(f"  {len(data)} relation types")
        logging.info(f"adding relations for {entity_type}")
        save_relations(data, entity_type, nx_graph)

        
class TemplateFunctions:
    ##
    ## data template functions
    ##
    ## take uri selector as first argument
    ## return list of 
    ## (uri, value) for attributes
    ## (uri, uri) for relations
    ##
    def att_proppath(urisel, path, store):
        query = '''
        select ?uri ?attr
        where {
            ?uri a %s .
            ?uri %s ?attr .
        }'''%(urisel, path)
        res = store.query(query, initNs=nsPrefixes)
    
        # return uri, attribute pairs
        attrs = [(r.uri, r.attr) for r in res]
        return attrs
    
    def att_proppath_slice(urisel, arg, store):
        path, start, end = arg
        query = '''
        select ?uri ?attr
        where {
            ?uri a %s .
            ?uri %s ?attr .
        }'''%(urisel, path)
        res = store.query(query, initNs=nsPrefixes)
    
        # return uri, attribute pairs
        attrs = [(r.uri, r.attr[start:end]) for r in res]
        return attrs
    
    def att_proppath_longlat(urisel, arg, store):
        """return longitude or latitude of the WKT object at the property path.
           arg: (path, "long"|"lat")
        """
        path, longorlat = arg
        query = '''
        select ?uri ?attr
        where {
            ?uri a %s .
            ?uri %s ?attr .
        }'''%(urisel, path)
        res = store.query(query, initNs=nsPrefixes)
    
        # return uri, attribute pairs
        attrs = []
        for r in res:
            geom = shapely.from_wkt(str(r.attr))
            if longorlat == 'long':
                attrs.append((r.uri, geom.centroid.x))
            else:
                attrs.append((r.uri, geom.centroid.y))

        return attrs
    
    def att_where(urisel, where, store):
        """return value matched by the where clause.
           where can be a string or list of strings.
           arg: [ where ]
        """
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri ?attr
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        # return uri, attribute pairs
        attrs = [(r.uri, r.attr) for r in res]
        return attrs
    
    def att_id_from_uri(urisel, sel, store):
        """get ismi_id in the uri"""
        if sel is None:
            sel = urisel
            
        query = '''
        select ?uri
        where {
            ?uri a %s .
        }'''%(sel)
        res = store.query(query, initNs=nsPrefixes)
    
        attrs = []
        for r in res:
            uri = r.uri
            ismi_id_match = ismi_object_id_pattern.search(uri)
            if ismi_id_match is not None:
                ismi_type = ismi_id_match.group(1)
                ismi_id = ismi_id_match.group(2)
                # return uri, id pairs
                attrs.append((uri, ismi_id))
                # save uri, id mapping
                if uri in uri_ismi_id_map:
                    if uri_ismi_id_map[uri] != ismi_id:
                        logging.error(f"uri {uri} with different ismi_id! {ismi_id} vs {uri_ismi_id_map[uri]}")
                else:
                    uri_ismi_id_map[uri] = ismi_id

            else:
                logging.warning(f"unable to get att_id_from_uri for uri {uri}")
        
        return attrs

    def att_alias_id(urisel, sel, store):
        """get ismi_id in the uri for alias"""
        query = '''
        select ?uri ?seealso
        where {
            ?uri a %s .
            optional {
                ?seealso skos:closeMatch ?uri .
            }
        }'''%(urisel)
        res = store.query(query, initNs=nsPrefixes)
        attrs = []
        for r in res:
            ma = ismi_alias_id_pattern.search(r.uri)
            if ma is not None:
                ismi_type = ma.group(1)
                parent_id = ma.group(2)
                alias_type = ma.group(3)
                alias_rest = ma.group(4)
                if alias_type in {'name', 'title', 'identifier'}:
                    # alias id in rest
                    if alias_rest.startswith('/'):
                        ismi_id = alias_rest[1:]
                    else:
                        logging.error(f"Invalid ALIAS identifier '{alias_rest}' in uri {r.uri}!")
                        continue
                    
                    if r.seealso:
                        # alias author/title_written_as uses id from witness
                        ms = ismi_alias_id_pattern.match(r.seealso)
                        if ms:
                            ismi_id = ismi_id + '-' + ms.group(3)
                        else:
                            logging.error(f"Unknown seeAlso ALIAS uri {r.seealso}!")
                    
                elif alias_type in {'title-as-written','author-as-written'}:
                    ismi_id = parent_id + '-' + alias_type
                    
                else:
                    logging.error(f"Unknown ALIAS type '{alias_type}' in uri {r.uri}!")
                    continue

                # return uri, id pairs
                attrs.append((r.uri, ismi_id))
                # save uri, id mapping
                if r.uri in uri_ismi_id_map:
                    if uri_ismi_id_map[r.uri] != ismi_id:
                        logging.error(f"uri {r.uri} with different ismi_id! {ismi_id} vs {uri_ismi_id_map[r.uri]}")
                else:
                    uri_ismi_id_map[r.uri] = ismi_id
                
            else:
                logging.error(f"Unknown ALIAS uri {r.uri}!")
        
        return attrs

    def att_ismi_id(urisel, sel, store):
        """get ismi_id type"""
        query = '''
        select ?uri ?attr
        where {
            ?uri a %s ;
                crm:P1_is_identified_by ?id .
            ?id crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/identifier/ismi-id> ;
                rdfs:label ?attr .
        }'''%(urisel)
        res = store.query(query, initNs=nsPrefixes)
    
        # return uri, attribute pairs
        attrs = [(r.uri, r.attr) for r in res]
        for r in res:
            # save uri, id mapping
            if r.uri in uri_ismi_id_map:
                if uri_ismi_id_map[r.uri] != str(r.attr):
                    logging.error(f"uri {r.uri} with different ismi_id! {r.attr} vs {uri_ismi_id_map[r.uri]}")
            else:
                uri_ismi_id_map[r.uri] = str(r.attr)
                    
        return attrs
    
    def att_public(urisel, sel, store):
        """get ismi-public type"""
        # match optional so we get all uris
        query = '''
        select ?uri ?attr
        where {
            ?uri a %s .
            optional {
                ?uri crm:P2_has_type ?attr .
            }
        }'''%(urisel)
        res = store.query(query, initNs=nsPrefixes)
    
        attrs = {}
        for r in res:
            public = False
            if r.attr:
                match = ismi_public_pattern.search(r.attr)
                if match is not None:
                    val = match.group(1)
                    if val == 'is-public':
                        public = True
    
            # save public value per uri
            if not r.uri in attrs:
                attrs[r.uri] = public
            else:
                attrs[r.uri] = attrs[r.uri] or public
        
        # return uri, value pairs
        return [(u, p) for u, p in attrs.items()]
    
    def att_match_uri(urisel, arg, store):
        """match regex to the uri"""
        if isinstance(arg, str):
            pattern = arg
            gid = 1
        else:
            pattern, gid = arg
            
        query = '''
        select ?uri
        where {
            ?uri a %s .
        }'''%(urisel)
        res = store.query(query, initNs=nsPrefixes)
    
        attrs = []
        for r in res:
            match = re.search(pattern, r.uri)
            if match is not None:
                val = match.group(gid)
                # return uri, value pairs
                attrs.append((r.uri, val))
        
        return attrs
    
    def att_proppath_match_uri(urisel, arg, store):
        """match regex to the uri at the end of a proppath.
           arg: (path, pattern)
        """
        path, pattern = arg
        query = '''
        select ?uri ?attr
        where {
            ?uri a %s .
            ?uri %s ?attr .
        }'''%(urisel, path)
        res = store.query(query, initNs=nsPrefixes)
    
        attrs = []
        for r in res:
            match = re.search(pattern, r.attr)
            if match is not None:
                val = match.group(1)
                # return uri, value pairs
                attrs.append((r.uri, val))
        
        return attrs
    
    def att_proppath_match_uri_label(urisel, arg, store):
        """match regex to the uri at the end of a proppath and return its label.
           arg: (path, pattern)
        """
        path, pattern = arg
        query = '''
        select ?uri ?attr ?label
        where {
            ?uri a %s .
            ?uri %s ?attr .
            ?attr rdfs:label ?label .
        }'''%(urisel, path)
        res = store.query(query, initNs=nsPrefixes)
    
        attrs = []
        for r in res:
            match = re.search(pattern, r.attr)
            if match is not None:
                # return uri, value pairs
                attrs.append((r.uri, r.label))
        
        return attrs
    
    def att_const(urisel, arg, store):
        """return the constant arg as value.
           arg: arg
        """
        query = '''
        select ?uri
        where {
            ?uri a %s .
        }'''%(urisel)
        res = store.query(query, initNs=nsPrefixes)
    
        attrs = []
        for r in res:
            attrs.append((r.uri, arg))
            
        return attrs
    
    def rel_proppath(urisel, path, store):
        """return the id of the uri matched by the SPARQL property path.
           arg: path
        """ 
        query = '''
        select ?uri ?target
        where {
            ?uri a %s .
            ?uri %s ?target .
        }'''%(urisel, path)
        res = store.query(query, initNs=nsPrefixes)
    
        # return ismi_id, ismi_id pairs
        rels = [(get_id_from_uri(r.uri), get_id_from_uri(r.target)) for r in res]
        return rels
    
    def rel_proppath_match_target(urisel, arg, store):
        """return the id of the regex matched to the uri at the end of the property path.
           arg: (path, pattern)
        """
        path, pattern = arg
        query = '''
        select ?uri ?target
        where {
            ?uri a %s .
            ?uri %s ?target .
        }'''%(urisel, path)
        res = store.query(query, initNs=nsPrefixes)
    
        rels = []
        for r in res:
            match = re.search(pattern, r.target)
            if match is not None:
                # return ismi_id, ismi_id pairs
                rels.append((get_id_from_uri(r.uri), match.group(1)))
        
        return rels
    
    def rel_where(urisel, where, store):
        """match uri and target using a where clause.
           arg: where (string or list)
        """
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri ?target
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        # return ismi_id, ismi_id pairs
        rels = [(get_id_from_uri(r.uri), get_id_from_uri(r.target)) for r in res]
        return rels
    
    def rel_where_id(urisel, where, store):
        """select uri and literal target_id from the where clause.
           (use when id is not in target uri)
        """
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri ?target_id
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        # return ismi_id, ismi_id pairs
        rels = [(get_id_from_uri(r.uri), str(r.target_id)) for r in res]
        return rels
    
    def rel_id_where(urisel, where, store):
        """select literal uri_id and target from the where clause.
           (use when id is not in uri path)
        """
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri_id ?target
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        # return ismi_id, ismi_id pairs
        rels = [(str(r.uri_id), get_id_from_uri(r.target)) for r in res]
        return rels
    
    def rel_where_match_src(urisel, arg, store):
        """match regex to the (source) ?uri of the where clause.
           arg: (where, pattern)
        """
        where, pattern = arg
        if isinstance(pattern, str):
            gid = 1
        else:
            pattern, gid = pattern
    
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri ?target
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        rels = []
        for r in res:
            match = re.search(pattern, r.uri)
            if match is not None:
                # return ismi_id, ismi_id pairs
                rels.append((match.group(gid), get_id_from_uri(r.target)))
                
        return rels
    
    def rel_where_match_tar(urisel, arg, store):
        """match regex to the (target) ?target of the where clause.
           arg: (where, pattern)
        """
        where, pattern = arg
        if isinstance(pattern, str):
            gid = 1
        else:
            pattern, gid = pattern
    
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri ?target
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        rels = []
        for r in res:
            match = re.search(pattern, r.target)
            if match is not None:
                # return ismi_id, ismi_id pairs
                rels.append((get_id_from_uri(r.uri), match.group(gid)))
                
        return rels
    
    def rel_where_match_src_alias(urisel, arg, store):
        """match regex to the (source) ?uri of the where clause.
           the source is an ALIAS and has an optional relation rel.
           arg: (where, pattern)
        """
        where, pattern = arg
        if isinstance(pattern, str):
            gid = 1
        else:
            pattern, gid = pattern
    
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri ?target ?rel
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        rels = []
        for r in res:
            m = re.search(pattern, r.uri)
            if m is not None:
                tar_id = get_id_from_uri(r.target)
                # use normal id from uri
                src_id = m.group(gid)
                if r.rel:
                    # alias author/title_written_as uses id from witness - create id
                    ms = ismi_alias_id_pattern.match(r.rel)
                    if ms:
                        src_id = src_id + '-' + ms.group(3)
                    else:
                        logging.error(f"Unknown seeAlso ALIAS uri {r.rel}!")
                        
                # return ismi_id, ismi_id pairs
                rels.append((src_id, tar_id))
                
        return rels
    
    def rel_where_alias_tar(urisel, where, store):
        """match alias to the ?target of the where clause.
        """
        if isinstance(where, list):
            where = ' \n'.join(where)
            
        query = '''
        select ?uri ?target
        where {
            ?uri a %s .
            %s
        }'''%(urisel, where)
        res = store.query(query, initNs=nsPrefixes)
    
        rels = []
        for r in res:
            match = ismi_alias_id_pattern.search(r.target)
            if match is not None:
                ismi_type = match.group(1)
                parent_id = match.group(2)
                alias_type = match.group(3)
                alias_rest = match.group(4)
                if alias_type in {'name', 'title', 'identifier'}:
                    # alias id in rest
                    if alias_rest.startswith('/'):
                        ismi_id = alias_rest[1:]
                    else:
                        logging.error(f"Invalid ALIAS identifier '{alias_rest}' in target {r.target} relation from {r.uri}!")
                        continue
                    
                elif alias_type in {'title-as-written','author-as-written'}:
                    ismi_id = parent_id + '-' + alias_type
                    
                else:
                    logging.error(f"Unknown ALIAS type '{alias_type}' in target {r.target} relation from {r.uri}!")
                    continue

                # return ismi_id, ismi_id pairs
                rels.append((get_id_from_uri(r.uri), ismi_id))

            else:
                logging.error(f"Unknown ALIAS target uri {r.target}!")
                
        return rels
    

## main
argp = argparse.ArgumentParser(description='Copy ISMI graph from RDF to networkx pickle.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('outfile', nargs='?', help='Output graph in gpickle format',
                  default='ismi-graph.gpickle')
argp.add_argument('-f', '--file', dest='rdf_file', help='RDF file')
argp.add_argument('-u', '--url', dest='sparql_url', help='triplestore SPARQL endpoint URL',
                  default='http://localhost:8082/blazegraph/namespace/kb/sparql')
argp.add_argument('--mapping', dest='mapping_file', help='JSON mapping definition file',
                  default='ismirdf2model.json')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
argp.add_argument('--log-to', dest='log_to', default='STDERR', 
                  help='Log destination (STDOUT, STDERR).')
args = argp.parse_args()

if args.log_to == 'STDOUT':
    stream = sys.stdout
else:
    stream = sys.stderr
    
logging.basicConfig(level=args.loglevel, stream=stream)

# load JSON mapping templates
mapping_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), args.mapping_file)
logging.info(f"loading mapping templates from {mapping_file_path}")
with open(mapping_file_path) as mf:
    mapping = json.load(mf)

if not mapping:
    logging.error(f"Unable to load mapping file {mapping_file_path}!")
    sys.exit(1)
else:
    mapversion = mapping['version']
    del mapping['version']
    logging.info(f"  mapping version {mapversion}")

if args.rdf_file:
    # load RDF file
    store = Dataset()
    logging.info(f"loading RDF file {args.rdf_file}")
    store.parse(args.rdf_file)
    
else:
    # connect to triplestore
    store = sparqlstore.SPARQLStore()
    logging.info(f"opening connection to SPARQL endpoint {args.sparql_url}")
    store.open(args.sparql_url)

# networkx graph
nx_graph = networkx.MultiDiGraph()

# run all mappings
process_all_entities(store, mapping, nx_graph)
logging.info(f"graph info: {nx_graph}")

# save resulting graph
logging.info(f"saving graph as {args.outfile}")
save_graph(args.outfile, nx_graph)
