from rdflib import Graph, Literal, URIRef, Dataset
from rdflib.namespace import Namespace
from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore
import logging
import requests
import re
import sys
import argparse

__version__ = '1.4'

# update even if not necessarily required
force_update = False

##
## namespaces
##
rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
crmNs = Namespace('http://www.cidoc-crm.org/cidoc-crm/')
digNs = Namespace('http://www.ics.forth.gr/isl/CRMdig/')
frbrNs = Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/')
skosNs = Namespace('http://www.w3.org/2004/02/skos/core#')
ismiNs = Namespace('http://ontologies.mpiwg-berlin.mpg.de/ismi/')
nsPrefixes = {'crm': crmNs, 'dig': digNs, 'frbr': frbrNs, 'skos': skosNs, 'ismi': ismiNs}
#nsPrefixes = {'crm': crmNs, 'dig': digNs, 'frbr': frbrNs, 'ismi': ismiNs}

##
## patterns
##
ismi_object_id_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/([-\w]+)/([-\w]+)')
ismi_type_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/type/([-\w]+)/([-\w]+)')
alias_name_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/person/(\d+)/name/(\d+)')
author_written_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/witness/(\d+)/author-as-written')
alias_title_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/text/(\d+)/title/(\d+)')
title_written_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/witness/(\d+)/title-as-written')
ismi_bibliography_pattern = re.compile(r'http://ismi.mpiwg-berlin.mpg.de/bibliography/(\d+)')

##
## URLs
##
ismi_biblio_info_url = 'https://ismi.mpiwg-berlin.mpg.de/api/biblio-info/'
ismi_biblio_list_url = 'https://ismi.mpiwg-berlin.mpg.de/api/biblio-info-all'
witness_graph_uri='http://content.mpiwg-berlin.mpg.de/ns/ismi/witness/%s/container/context'
external_graph_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/external/container/context'
ismi_group_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/%s'
ismi_group_graph_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/%s/container/context'
bibliography_base_uri = 'http://ismi.mpiwg-berlin.mpg.de/bibliography'
bibliography_graph_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/bibliography/container/context'


##
## connection to triplestore
##
store = Dataset('SPARQLUpdateStore')


def process_witness_has_author_written_as():
    """
    Problem:
        ALIAS-is_alias_name_of->PERSON uses the alias id of the WITNESS from
        WITNESS-has_author_written_as->ALIAS 
    Solution:
        find matching alias_name, add label if necessary, and add seeAlso link
    """
    # collect all person alias names (uri, value) and store by alias id
    name_aliases = {}
    query = '''select ?person ?alias ?value
    where {
        ?person a ismi:Person .
        ?person crm:P1_is_identified_by ?alias .
        ?alias crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/name/alias-name> .
        ?alias rdfs:label ?value .
    }'''
    res = store.query(query)
    for r in res:
        match = alias_name_pattern.match(r.alias)
        if match:
            alias_id = match.group(2)
            name_aliases[alias_id] = (r.alias, r.value)
    
    # select all author_as_written
    query = '''select ?wit ?alias ?val ?alias2
    where {
        ?wit a ismi:Witness .
        ?wit crm:P128_carries / frbr:R15_has_fragment ?alias .
        ?alias crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/fragment/author-as-written> .
        optional {
            ?alias rdfs:label ?val .
        }
        optional {
            ?alias skos:closeMatch ?alias2
        }
    }'''
    res = store.query(query)
    cnt = 0
    new_quads = []
    for r in res:
        if r.val is not None and r.alias2 is not None:
            # everything fixed already
            continue
        
        match = author_written_pattern.match(r.alias)
        if match:
            witness_id = match.group(1)
            if witness_id not in name_aliases:
                logging.warning(f"author_written_as alias {r.alias} not found as alias_name_of!")
                if r.val is None:
                    raise RuntimeError(f"author_written_as alias {r.alias} without matching alias_name_of has no label!")
                
                continue
            
            # get witness graph
            graph = store.get_context(URIRef(witness_graph_uri%witness_id))
            # get alias
            name_alias_uri, value = name_aliases[witness_id]
            if r.val is None:
                # add value as label
                new_quads.append((r.alias, rdfsNs.label, value, graph))
                
            # add relation to alias name
            new_quads.append((r.alias, skosNs.closeMatch, name_alias_uri, graph))
            cnt += 1
            
        else:
            logging.error(f"author_written_as uri {r.alias} did not match alias id pattern!")

    if new_quads:
        store.addN(new_quads)
        
    logging.info(f"updated {cnt} of {len(res)} author aliases.")


def process_witness_has_title_written_as():
    """
    Problem:
        ALIAS-is_alias_title_of->TEXT uses the alias id of the WITNESS from
        WITNESS-has_title_written_as->ALIAS 
    Solution:
        find matching alias_title, copy label if necessary, and add seeAlso link
    """
    # collect all person alias names (uri, value) and store by alias id
    title_aliases = {}
    query = '''select ?text ?alias ?value
    where {
        ?text a ismi:Text .
        ?text crm:P102_has_title ?alias .
        ?alias crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/title/alias-title> .
        ?alias rdfs:label ?value .
    }'''
    res = store.query(query)
    for r in res:
        match = alias_title_pattern.match(r.alias)
        if match:
            witness_id = match.group(2)
            title_aliases[witness_id] = (r.alias, r.value)
    
    # select all author_as_written
    query = '''select ?wit ?alias ?val ?alias2
    where {
        ?wit a ismi:Witness .
        ?wit crm:P128_carries / frbr:R15_has_fragment ?alias .
        ?alias crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/fragment/title-as-written> .
        optional {
            ?alias rdfs:label ?val .
        }
        optional {
            ?alias skos:closeMatch ?alias2
        }
    }'''
    res = store.query(query)
    new_quads = []
    cnt = 0
    for r in res:
        if r.val is not None and r.alias2 is not None:
            # everything fixed already
            continue
        
        match = title_written_pattern.match(r.alias)
        if match:
            witness_id = match.group(1)
            if witness_id not in title_aliases:
                logging.warning(f"title_written_as alias {r.alias} not found as alias_title_of!")
                if r.val is None:
                    raise RuntimeError(f"title_written_as alias {r.alias} without matching alias_title_of has no label!")
                continue

            # get witness graph
            graph = store.get_context(URIRef(witness_graph_uri%witness_id))
            # get alias
            title_alias_uri, value = title_aliases[witness_id]
            if r.val is None:
                # add value as label
                new_quads.append((r.alias, rdfsNs.label, value, graph))
                
            # add relation to alias name
            new_quads.append((r.alias, skosNs.closeMatch, title_alias_uri, graph))
            cnt += 1
            
        else:
            logging.error(f"title_written_as uri did not match alias id pattern!")

    if new_quads:
        store.addN(new_quads)
        
    logging.info(f"updated {cnt} of {len(res)} title aliases.")


def process_ismi_bibliography_labels():
    """
    Update all ISMI bibliography URIs from the ismi server.
    """
    http_session = requests.Session()
    biblio_info = []
    # fetch all biblio_ref from ismi server
    page = 0
    logging.info(f"loading ismi bibliography...")
    while True:
        http_response = http_session.get(ismi_biblio_list_url, params={'items_per_page': 100, 'page': page})
        biblio_data = http_response.json()
        if not biblio_data or page > 100:
            break
        else:
            biblio_info.extend(biblio_data)
            logging.info(f"  loading {(page + 1) * 100}...")
            page += 1
    
    if len(biblio_info) == 0 or page > 100:
        logging.error(f"unable to load bibliography data!")
        return
    
    # remove all triples from bibliography graph
    graph = store.get_context(URIRef(bibliography_graph_uri))
    store.remove_context(graph)
    # new context for bibliography graph
    graph = store.get_context(URIRef(bibliography_graph_uri))
    new_quads = []
    bibentry_uri = URIRef('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/uri/ismi-bibliography-entry')
    
    cnt = 0
    for bib in biblio_info:
        # create E73_Information_Object entries for all biblio entries
        uri = URIRef(bibliography_base_uri + '/' + bib['biblio_id'])
        # add crm:E73 type and crm:P2_has_type
        new_quads.append((uri, rdfNs.type, crmNs.E73_Information_Object, graph))
        new_quads.append((uri, crmNs.P2_has_type, bibentry_uri, graph))
        # add reference as label
        ref = f"{bib['citation_text'].strip()} {{#{bib['biblio_id']}}}"
        logging.debug(f"reference label={repr(ref)}")
        new_quads.append((uri, rdfsNs.label, Literal(ref), graph))
        cnt += 1

        # batch add quads
        if len(new_quads) > 100:
            logging.info(f"  adding {cnt}...")
            store.addN(new_quads)
            new_quads = []
            
    # add last batch
    if new_quads:
        logging.info(f"  adding {cnt}...")
        store.addN(new_quads)
    
    logging.info(f"updated {cnt} bibliography URLs.")


def process_type_groups():
    """
    Problem:
        Some E55_Types like /ismi/type/study-type/xxx or /ismi/type/text-type/xxx should be grouped
        using skos:Collection-skos:member->skos:Concept but this is difficult in a 3M mapping.
    Solution:
        Add skos:Collection-skos:member->skos:Concept to specific types.
        Create skos:Collections.
    """
    # types that should be grouped in one graph
    grouped_types = {
        'type/place-type',
        'type/script-type',
        'type/source-of-information',
        'type/study-type',
        'type/text-type',
        'type/text-relation',
        'type/transfer-type',
        'type/writing-surface',
        'type/codex-binding',
        'type/language',
        'role',
    }
    t_cnt = 0
    c_cnt = 0
    new_quads = []
    query = '''select ?type ?collection
    where {
        {
            ?type a crm:E55_Type .
        } union {
            ?type a crm:E56_Language .
        }
        filter(contains(str(?type), "/ismi/%s")).
        optional {
          ?collection skos:member ?type.
        }
    }'''
    for type_part in grouped_types:
        # named graph for type group / collection
        group_graph = store.get_context(URIRef(ismi_group_graph_uri%type_part))
        # uri for skos:Collection
        group_uri = URIRef(ismi_group_uri%type_part)
        create_collection = False
        res = store.query(query%type_part)
        for r in res:
            collection_uri = r.collection
            if not collection_uri:
                # not member of collection
                create_collection = True
                t_cnt += 1
                # add skos:Concept type and skos:member relation
                new_quads.append((r.type, rdfNs.type, skosNs.Concept, group_graph))
                new_quads.append((group_uri, skosNs.member, r.type, group_graph))
             
        if create_collection:
            # add skos:Collection
            c_cnt += 1
            new_quads.append((group_uri, rdfNs.type, skosNs.Collection, group_graph))
            new_quads.append((group_uri, rdfsNs.label, Literal(type_part.replace('type/', '')), group_graph))

    if new_quads:
        store.addN(new_quads)
    
    logging.info(f"added {t_cnt} types to ({c_cnt} new) skos:Collections.")

## 
## main
##
argp = argparse.ArgumentParser(description='Reads and processes newly imported ISMI-CIDOC RDF in triple store.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('sparql_url', nargs='?', help='triplestore SPARQL (update) endpoint URL',
                  default='http://localhost:8082/blazegraph/namespace/kb/sparql')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

logging.basicConfig(level=args.loglevel)

# triplestore address
sparql_endpoint = args.sparql_url

## connect to triplestore
store.open((sparql_endpoint, sparql_endpoint))
for pref in nsPrefixes:
    store.bind(pref, nsPrefixes[pref])

logging.info("processing witness-has_author_written_as...")
process_witness_has_author_written_as()

logging.info("processing witness-has_title_written_as...")
process_witness_has_title_written_as()

logging.info("processing type groups...")
process_type_groups()

logging.info("processing ismi bibliography labels...")
process_ismi_bibliography_labels()
