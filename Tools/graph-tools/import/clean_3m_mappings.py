import xml.etree.ElementTree as ET
import sys

print("Fix 3M XML mapping file to be used with X3ML engine.")

# filenames
input_fn = "mapping.xml"
output_fn = "mapping-fixed.x3ml"

# parse command line parameters
if len(sys.argv) > 1:
    input_fn = sys.argv[1]

if len(sys.argv) > 2:
    output_fn = sys.argv[2]

# parse XML file
print(f"parsing XML file {input_fn}")
tree = ET.parse(input_fn)
root = tree.getroot()

# remove x3ml/admin
admin_elem = root.find('admin')
if admin_elem:
    print("removing x3ml/admin tag")
    root.remove(admin_elem)
    
# remove x3ml/info/source/source_info/namespaces
sinfo_elem = root.find('info/source/source_info')
if sinfo_elem:
    sinfons_elem = sinfo_elem.find('namespaces')
    if sinfons_elem:
        print("removing x3ml/info/source/source_info/namespaces tag")
        sinfo_elem.remove(sinfons_elem)

print(f"writing file {output_fn}")    
tree.write(output_fn, "utf-8")
