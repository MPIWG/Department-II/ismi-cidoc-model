from rdflib import Graph, graph
from rdflib import ConjunctiveGraph
from rdflib import URIRef, Literal
from rdflib.namespace import Namespace
from rdflib.namespace import XSD
import logging
import os
import sys
import re
import datetime
import argparse

__version__ = '1.2.1'

# RDF namespaces
rsFieldDefNs = Namespace('http://www.researchspace.org/resource/system/fields/')
rsFieldConNs = Namespace('http://www.researchspace.org/resource/system/')
mpFieldDefNs = Namespace('http://www.metaphacts.com/ontology/fields#')
mpFieldConNs = Namespace('http://www.metaphacts.com/ontologies/platform#')
rsUserNs = Namespace('http://www.researchspace.org/resource/user/')
mpUserNs = Namespace('http://www.metaphacts.com/resource/user/')
platformNs = rsFieldConNs
userNs = rsUserNs
ldpNs = Namespace('http://www.w3.org/ns/ldp#')
rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
xsdNs = Namespace('http://www.w3.org/2001/XMLSchema#')
provNs = Namespace('http://www.w3.org/ns/prov#')

# ismi RDF URI patterns
ismi_object_id_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/([-\w]+)/([-\w]+)')
ismi_object_graph_fmt = "http://content.mpiwg-berlin.mpg.de/ns/ismi/%s/%s/container/context"
ismi_subtype_graph_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/type/%s/container/context'
ismi_group_graph_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/%s/container/context'
other_graph_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi/external/container/context'

# ISMI types (uri parts) that should not get their own named graph
graphless_types = {
    'reference', 
    'misattribution', 
    'misidentification', 
    'transfer-event', 
    'study-event',
    'copy-event'
}

# non-type types that should be grouped in one graph
grouped_types = {
    'role'
}

# mapping of (non-ismi) uris to graphs
ext_uri_graph_map = {
    'http://ismi.mpiwg-berlin.mpg.de/bibliography/': 'http://content.mpiwg-berlin.mpg.de/ns/ismi/bibliography/container/context'
}


def convert_triples(ingraph, outgraphs):
    """
    Convert triples in ingraph to named graphs in outgraphs.
    
    Creates graph URI from the subject of each triple:
    * use subject URI if it is an ISMI URI
    * search for subject URI as object in other triples and use that subject URI for graphless_types
    * use other_graph_uri otherwise
    """
    for sub,pred,obj in ingraph:
        graph_uri = None
        
        # match ismi object type and id in the uri
        match = ismi_object_id_pattern.search(sub)        
        if match is not None:
            ismi_type = match.group(1)
            ismi_id = match.group(2)
            #
            # subject is a ismi type
            #
            if ismi_type == 'type':
                # put in graph per subtype
                graph_uri = URIRef(ismi_subtype_graph_uri%ismi_id)
             
            #
            # subject should not get its own graph
            #
            elif ismi_type in graphless_types:
                # put in related graph
                graph_uri = _find_related_graph(sub, ingraph)
                
                if graph_uri is None:
                    logging.warning("no related ismi subject found for graphless subject %s!"%sub)
                    graph_uri = URIRef(other_graph_uri)
                    outgraphs.add((sub, pred, obj, graph_uri))
            
            #
            # subject should be in a group graph
            #
            elif ismi_type in grouped_types:
                # put in group graph
                graph_uri = URIRef(ismi_group_graph_uri%ismi_type)
            
            #
            # subject gets its own graph
            #
            else:
                # create graph uri using type and id
                graph_uri = URIRef(ismi_object_graph_fmt%(ismi_type, ismi_id))
                
            # save quad
            outgraphs.add((sub, pred, obj, graph_uri))

        else:
            #
            # non-ismi subject
            #
            # check for specific graph pattern
            for prefix in ext_uri_graph_map.keys():
                if (sub.startswith(prefix)):
                    graph_uri = URIRef(ext_uri_graph_map[prefix])
                    break
            
            if graph_uri is None:
                # add to othergraph
                graph_uri = URIRef(other_graph_uri)
                
            outgraphs.add((sub, pred, obj, graph_uri))


def _find_related_graph(subject, graph):
    graph_uri = None
    # check all triples with the subject's uri as object
    for s, p, o in graph.triples((None, None, subject)):
        # match other subject uri
        match = ismi_object_id_pattern.search(s)
        if match is not None:
            ismi_type = match.group(1)
            ismi_id = match.group(2)
            
            if graph_uri is not None:
                new_graph_uri = URIRef(ismi_object_graph_fmt%(ismi_type, ismi_id))
                if graph_uri != new_graph_uri:
                    logging.debug("graphless subject %s related to multiple subjects! (first=%s, new=%s)" % (subject, graph_uri, match.group(0)))
                    
                # skip this subject
                continue

            # check if related type is graphless itself
            if ismi_type in graphless_types:
                # put in related graph
                graph_uri = _find_related_graph(s, graph)
            
            else:
                graph_uri = URIRef(ismi_object_graph_fmt%(ismi_type, ismi_id))

    return graph_uri


def add_ldp_triples(graphs):
    """
    Add metaphactory LDP triples to all named graphs in graphs.
    """

    # add global Metaphactory form container graph
    formCtx = graphs.get_context(platformNs['Container/context'])
    # add form container definition
    formCtx.add((platformNs.formContainer, rdfsNs.label, Literal("Form Container")))
    formCtx.add((platformNs.formContainer, provNs.wasAttributedTo, userNs.admin))
    timestamp = datetime.datetime.now(datetime.timezone.utc).isoformat()
    formCtx.add((platformNs.formContainer, provNs.generatedAtTime, Literal(timestamp, datatype=XSD.dateTime)))
    formCtx.add((platformNs.rootContainer, ldpNs.contains, platformNs.formContainer))
    formCtx.add((platformNs.formContainer, rdfNs.type, ldpNs.Container))
    formCtx.add((platformNs.formContainer, rdfsNs.comment, Literal("Container to store statements created by forms.")))
    formCtx.add((platformNs.formContainer, rdfNs.type, ldpNs.Resource))
    formCtx.add((platformNs.formContainer, rdfNs.type, provNs.Entity))

    # link all named graphs to form container
    for ctx in graphs.contexts():
        ctx_uri = ctx.identifier
        container_uri = URIRef(str(ctx_uri).replace('/container/context', '/container'))
        # Platform:formContainer ldpNs:contains ?container .
        graphs.add((platformNs.formContainer, ldpNs.contains, container_uri, ctx_uri))
        # ?container a ldpNs:Container, ldpNs:Resource .
        #graphs.add((container_uri, rdfNs.type, ldpNs.Container, ctx_uri))
        graphs.add((container_uri, rdfNs.type, ldpNs.Resource, ctx_uri))


def convert_files(infilenames, outfilename):
    """
    Convert all files from infilenames into outfilename.
    """
    # unnamed input graph
    input_graph = Graph()
    # named output graphs
    named_graphs = ConjunctiveGraph()

    for infilename in infilenames:
        logging.info("reading %s (starting with %s triples)..."%(infilename, len(input_graph)))
        input_graph.parse(infilename, format='nt')

    logging.info("processing %s triples..."%len(input_graph))
    convert_triples(input_graph, named_graphs)
    logging.info("created %s named triples"%len(named_graphs))
    logging.info("adding LDP triples...")
    add_ldp_triples(named_graphs)
    logging.info("writing %s... (%s)"%(outfilename, len(named_graphs)))
    named_graphs.serialize(outfilename, format='nquads')

## 
## main
##
argp = argparse.ArgumentParser(description='Read ISMI-CIDOC RDF triple files and add named graphs producing nquads.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('infile', nargs='+', 
                  help='RDF input file in ntriples format (if first input file is a directory reads all files in the directory)')
argp.add_argument('outfile', help='RDF output file in nquads format')
argp.add_argument('--flavor', dest='flavor', choices=['RS', 'MP'], default='RS', 
                  help='RS/MP flavor.')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

logging.basicConfig(level=args.loglevel)

if args.flavor == 'MP':
    # use Metaphacts URIs
    platformNs = mpFieldConNs
    userNs = mpUserNs

infiles = args.infile
outfile = args.outfile

# if first param is a directory use the files in the directory
if os.path.isdir(infiles[0]):
    dir = infiles[0]
    infiles = [os.path.join(dir, f) for f in os.listdir(infiles[0]) if not f.startswith('.')]

convert_files(infiles, outfile)
