from rdflib import ConjunctiveGraph, URIRef
from rdflib.namespace import Namespace

import sys
import logging
import argparse

__version__ = '1.5'

##
## namespaces
##
rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
crmNs = Namespace('http://www.cidoc-crm.org/cidoc-crm/')
digNs = Namespace('http://www.ics.forth.gr/isl/CRMdig/')
frbrNs = Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/')
skosNs = Namespace('http://www.w3.org/2004/02/skos/core#')
ismiNs = Namespace('http://ontologies.mpiwg-berlin.mpg.de/ismi/')
ismiIdTypeNs = Namespace('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/identifier/')
nsPrefixes = {'crm': crmNs, 'crmdig': digNs, 'frbroo': frbrNs, 'ismi': ismiNs,
              'ismiidtype': ismiIdTypeNs}

##
## check functions
##
allowed_subject_wo_class = {
    URIRef('http://www.researchspace.org/resource/system/rootContainer'),
    URIRef('http://content.mpiwg-berlin.mpg.de/ns/ismi/db/last-ismi-id')
}

def check_class(store, errors, warnings):
    """check that all subjects have a rdf:type"""
    query = '''SELECT DISTINCT ?subject
        WHERE {
          ?subject ?p ?o .
          FILTER NOT EXISTS { ?subject a ?class }
        }'''
    res = store.query(query)
    for r in res:
        uri = r.subject
        if uri in allowed_subject_wo_class:
            continue
        else:
            msg = 'subject without class'
            logging.debug(msg)
            errors.append(('check_subject_class', msg, uri))

    return errors, warnings

def check_ismi_id(store, errors, warnings):
    """check that all ismi_ids are unique"""
    query = '''SELECT DISTINCT ?subject ?ismi_id
        WHERE {
          ?subject crm:P1_is_identified_by ?id .
          ?id a crm:E42_Identifier ;
            crm:P2_has_type ismiidtype:ismi-id ;
            rdfs:label ?ismi_id .
        }'''
    res = store.query(query)
    ismi_ids = {}
    for r in res:
        uri = r.subject
        ismi_id = r.ismi_id
        if ismi_id in ismi_ids:
            msg = 'duplicate ismi_id'
            logging.debug(msg)
            errors.append(('check_ismi_id', msg, f"{ismi_id} in {uri} and {ismi_ids[ismi_id]}"))

        ismi_ids[ismi_id] = uri
        
    return errors, warnings

def check_person(store, errors, warnings):
    """check ismi:Person
    * has crm class
    * has ismi_id
    * has preferred_name
    """
    query = '''SELECT DISTINCT ?person ?crm ?ismi_id ?pref_name
        WHERE {
          ?person a ismi:Person .
          OPTIONAL {
            ?person a ?crm .
            FILTER (?crm = crm:E21_Person)
          }
          OPTIONAL {
            ?person crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?person crm:P1_is_identified_by ?name .
            ?name a crm:E41_Appellation ;
              crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/name/preferred-name> ;
              rdfs:label ?pref_name .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?pref_name))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.person
        if not r.crm:
            msg = 'person without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_person', msg, uri))

        if not r.ismi_id:
            msg = 'person without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_person', msg, uri))

        if not r.pref_name:
            msg = 'person without preferred_name'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_person', msg, uri))

    return errors, warnings

def check_text(store, errors, warnings):
    """check ismi:Text
    * has crm class
    * has ismi_id
    * has preferred_title
    * has author
    * has representative_expression
    """
    query = '''SELECT DISTINCT ?text ?crm ?ismi_id ?pref_title ?exp ?author
        WHERE {
          ?text a ismi:Text .
          OPTIONAL {
            ?text a ?crm .
            filter(?crm = frbroo:F14_Individual_Work)
          }
          OPTIONAL {
            ?text crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?text crm:P102_has_title ?title .
            ?title a crm:E35_Title ;
              crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/title/preferred-title> ;
              rdfs:label ?pref_title .
          }
          OPTIONAL {
            ?text frbroo:R40_has_representative_expression ?exp .
            ?exp a frbroo:F22_Self-Contained_Expression .
          }
          OPTIONAL {
            ?text frbroo:R16i_was_initiated_by ?creat .
            ?creat a frbroo:F27_Work_Conception ;
              crm:P14_carried_out_by ?author .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?pref_title) || !bound(?exp) || !bound(?author))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.text
        if not r.crm:
            msg = 'text without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_text', msg, uri))

        if not r.ismi_id:
            msg = 'text without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_text', msg, uri))

        if not r.pref_title:
            msg = 'text without preferred_title'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_text', msg, uri))

        if not r.exp:
            msg = 'text without representative expression'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_text', msg, uri))

        if not r.author:
            msg = 'text without author'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_text', msg, uri))

    return errors, warnings

def check_witness(store, errors, warnings):
    """check ismi:Witness
    * has crm class
    * has ismi_id
    * has expression / text
    * has codex
    """
    query = '''SELECT DISTINCT ?witness ?crm ?ismi_id ?text ?codex
        WHERE {
          ?witness a ismi:Witness .
          OPTIONAL {
            ?witness a ?crm .
            FILTER (?crm = frbroo:F4_Manifestation_Singleton)
          }
          OPTIONAL {
            ?witness crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?witness crm:P128_carries ?exp .
            ?exp a frbroo:F22_Self-Contained_Expression ;
              frbroo:R9i_realises ?text .
          }
          OPTIONAL {
            ?witness crm:P46i_forms_part_of ?codex .
            ?codex a crm:E84_Information_Carrier .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?text) || !bound(?codex))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.witness
        if not r.crm:
            msg = 'witness without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_witness', msg, uri))

        if not r.ismi_id:
            msg = 'witness without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_witness', msg, uri))

        if not r.text:
            msg = 'witness without text'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_witness', msg, uri))

        if not r.codex:
            msg = 'witness without codex'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_witness', msg, uri))

    return errors, warnings

def check_codex(store, errors, warnings):
    """check ismi:Codex
    * has crm class
    * has ismi_id
    * has witness
    * has collection
    """
    query = '''SELECT DISTINCT ?codex ?crm ?ismi_id ?pref_id ?wit ?coll
        WHERE {
          ?codex a ismi:Codex .
          OPTIONAL {
            ?codex a ?crm .
            filter(?crm = crm:E84_Information_Carrier)
          }
          OPTIONAL {
            ?codex crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?codex crm:P1_is_identified_by ?pid .
            ?pid a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:preferred-identifier ;
              rdfs:label ?pref_id .
          }
          OPTIONAL {
            ?wit crm:P46i_forms_part_of ?codex .
            ?wit a frbroo:F4_Manifestation_Singleton .
          }
          OPTIONAL {
            ?codex crm:P46i_forms_part_of ?coll .
            ?coll a crm:E78_Collection .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?pref_id) || !bound(?wit) || !bound(?coll))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.codex
        if not r.crm:
            msg = 'codex without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_codex', msg, uri))

        if not r.ismi_id:
            msg = 'codex without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_codex', msg, uri))

        if not r.pref_id:
            msg = 'codex without pref_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_codex', msg, uri))

        if not r.wit:
            msg = 'codex without witness'
            logging.debug(msg + ': %s', uri)
            warnings.append(('check_codex', msg, uri))

        if not r.coll:
            msg = 'codex without collection'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_codex', msg, uri))

    return errors, warnings

def check_collection(store, errors, warnings):
    """check ismi:Collection
    * has crm class
    * has ismi_id
    * has codex
    * has repository
    """
    query = '''SELECT DISTINCT ?collection ?crm ?ismi_id ?pref_name ?cod ?repo
        WHERE {
          ?collection a ismi:collection .
          OPTIONAL {
            ?collection a ?crm .
            filter(?crm = crm:E78_Collection)
          }
          OPTIONAL {
            ?collection crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?collection crm:P1_is_identified_by ?pid .
            ?pid a crm:E41_Appellation ;
              crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/name/preferred-name> ;
              rdfs:label ?pref_name .
          }
          OPTIONAL {
            ?cod crm:P46i_forms_part_of ?collection .
            ?cod a crm:E84_Information_Carrier .
          }
          OPTIONAL {
            ?collection crm:P49_has_former_or_current_keeper ?repo .
            ?repo a crm:E39_Actor .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?pref_name) || !bound(?cod) || !bound(?repo))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.collection
        if not r.crm:
            msg = 'collection without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_collection', msg, uri))

        if not r.ismi_id:
            msg = 'collection without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_collection', msg, uri))

        if not r.pref_name:
            msg = 'collection without pref_name'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_collection', msg, uri))

        if not r.cod:
            msg = 'collection without codex'
            logging.debug(msg + ': %s', uri)
            warnings.append(('check_collection', msg, uri))

        if not r.repo:
            msg = 'collection without repository'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_collection', msg, uri))

    return errors, warnings

def check_repository(store, errors, warnings):
    """check ismi:Repository
    * has crm class
    * has ismi_id
    * has collection
    * has place
    """
    query = '''SELECT DISTINCT ?repository ?crm ?ismi_id ?pref_name ?coll ?place
        WHERE {
          ?repository a ismi:Repository .
          OPTIONAL {
            ?repository a ?crm .
            filter(?crm = crm:E39_Actor)
          }
          OPTIONAL {
            ?repository crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?repository crm:P1_is_identified_by ?pid .
            ?pid a crm:E41_Appellation ;
              crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/name/preferred-name> ;
              rdfs:label ?pref_name .
          }
          OPTIONAL {
            ?coll crm:P49_has_former_or_current_keeper ?repository .
            ?coll a crm:E78_Collection .
          }
          OPTIONAL {
            ?repository crm:P74_has_current_or_former_residence ?place .
            ?place a crm:E53_Place .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?pref_name) || !bound(?coll) || !bound(?place))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.repository
        if not r.crm:
            msg = 'repository without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_repository', msg, uri))

        if not r.ismi_id:
            msg = 'repository without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_repository', msg, uri))

        if not r.pref_name:
            msg = 'repository without pref_name'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_repository', msg, uri))

        if not r.coll:
            msg = 'repository without collection'
            logging.debug(msg + ': %s', uri)
            warnings.append(('check_repository', msg, uri))

        if not r.place:
            msg = 'repository without place'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_repository', msg, uri))

    return errors, warnings

def check_place(store, errors, warnings):
    """check ismi:Place
    * has crm class
    * has ismi_id
    * has pref_name
    """
    query = '''SELECT DISTINCT ?place ?crm ?ismi_id ?pref_name ?ptype
        WHERE {
          ?place a ismi:Place .
          OPTIONAL {
            ?place a ?crm .
            filter(?crm = crm:E53_Place)
          }
          OPTIONAL {
            ?place crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?place crm:P1_is_identified_by ?pid .
            ?pid a crm:E41_Appellation ;
              crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/name/preferred-name> ;
              rdfs:label ?pref_name .
          }
          OPTIONAL {
            ?place crm:P2_has_type ?ptype .
            ?ptype a crm:E55_Type .
            <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/place-type> skos:member ?ptype .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?pref_name) || !bound(?ptype))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.place
        if not r.crm:
            msg = 'place without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_place', msg, uri))

        if not r.ismi_id:
            msg = 'place without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_place', msg, uri))

        if not r.pref_name:
            msg = 'place without pref_name'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_place', msg, uri))

        if not r.ptype:
            msg = 'place without place-type'
            logging.debug(msg + ': %s', uri)
            warnings.append(('check_place', msg, uri))

    return errors, warnings

def check_role(store, errors, warnings):
    """check ismi:Role
    * has crm class
    * has ismi_id
    * has pref_name
    """
    query = '''SELECT DISTINCT ?role ?crm ?skos ?ismi_id ?pref_name ?stype
        WHERE {
          ?role a ismi:role .
          OPTIONAL {
            ?role a ?crm .
            filter(?crm = crm:E55_Type)
          }
          OPTIONAL {
            ?role a ?skos .
            filter(?skos = skos:Concept)
          }
          OPTIONAL {
            ?role crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?role rdfs:label ?pref_name .
          }
          OPTIONAL {
            ?stype skos:member ?role .
            filter(?stype = <http://content.mpiwg-berlin.mpg.de/ns/ismi/role>)
          }
          FILTER (!bound(?crm) || !bound(?skos) || !bound(?ismi_id) || !bound(?pref_name) || !bound(?stype))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.role
        if not r.crm:
            msg = 'role without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_role', msg, uri))

        if not r.skos:
            msg = 'role without skos class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_role', msg, uri))

        if not r.ismi_id:
            msg = 'role without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_role', msg, uri))

        if not r.pref_name:
            msg = 'role without pref_name'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_role', msg, uri))

        if not r.stype:
            msg = 'role without skos:member'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_role', msg, uri))

    return errors, warnings

def check_subject(store, errors, warnings):
    """check ismi:Subject
    * has crm class
    * has ismi_id
    * has pref_name
    """
    query = '''SELECT DISTINCT ?subject ?crm ?skos ?ismi_id ?pref_name
        WHERE {
          ?subject a ismi:Subject .
          OPTIONAL {
            ?subject a ?crm .
            filter(?crm = crm:E73_Information_Object)
          }
          OPTIONAL {
            ?subject a ?skos .
            filter(?skos = skos:Concept)
          }
          OPTIONAL {
            ?subject crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?subject rdfs:label ?pref_name .
          }
          FILTER (!bound(?crm) || !bound(?skos) || !bound(?ismi_id) || !bound(?pref_name))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.subject
        if not r.crm:
            msg = 'subject without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_subject', msg, uri))

        if not r.skos:
            msg = 'subject without skos class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_subject', msg, uri))

        if not r.ismi_id:
            msg = 'subject without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_subject', msg, uri))

        if not r.pref_name:
            msg = 'subject without pref_name'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_subject', msg, uri))

    return errors, warnings

def check_transfer_event(store, errors, warnings):
    """check ismi:TransferEvent
    * has crm class
    * has ismi_id
    * has codex, destination
    """
    query = '''SELECT DISTINCT ?event ?crm ?ismi_id ?etype ?codex ?dest
        WHERE {
          ?event a ismi:TransferEvent .
          OPTIONAL {
            ?event a ?crm .
            filter(?crm = crm:E10_Transfer_of_Custody || ?crm = crm:E9_Move)
          }
          OPTIONAL {
            ?event crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?event crm:P2_has_type ?etype .
            ?etype a crm:E55_Type .
            <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/transfer-type> skos:member ?etype .
          }
          OPTIONAL {
            ?codex crm:P25i_moved_by | crm:P30i_custody_transferred_through ?event .
          }
          OPTIONAL {
            ?event crm:P26_moved_to | crm:P29_custody_received_by ?dest .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id)  || !bound(?etype) || !bound(?codex) || !bound(?dest))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.event
        if not r.crm:
            msg = 'transfer_event without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_transfer_event', msg, uri))

        if not r.ismi_id:
            msg = 'transfer_event without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_transfer_event', msg, uri))

        if not r.etype:
            msg = 'transfer_event without transfer_type'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_transfer_event', msg, uri))

        if not r.codex:
            msg = 'transfer_event without codex'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_transfer_event', msg, uri))

        if not r.dest:
            msg = 'transfer_event without destination'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_transfer_event', msg, uri))

    return errors, warnings

def check_study_event(store, errors, warnings):
    """check ismi:StudyEvent
    * has crm class
    * has ismi_id
    * has witness, person
    """
    query = '''SELECT DISTINCT ?event ?crm ?ismi_id ?etype ?witness ?person
        WHERE {
          ?event a ismi:StudyEvent .
          OPTIONAL {
            ?event a ?crm .
            filter(?crm = crm:E7_Activity)
          }
          OPTIONAL {
            ?event crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?event crm:P2_has_type ?etype .
            ?etype a crm:E55_Type .
            <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/study-type> skos:member ?etype .
          }
          OPTIONAL {
            ?witness crm:P16i_was_used_for ?event .
          }
          OPTIONAL {
            ?event crm:P01i_is_domain_of ?study .
            ?study a crm:PC14_carried_out_by ;
              crm:P14.1_in_the_role_of ?role ;
              crm:P02_has_range ?person .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?etype) || !bound(?witness) || !bound(?person))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.event
        if not r.crm:
            msg = 'study_event without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_study_event', msg, uri))

        if not r.ismi_id:
            msg = 'study_event without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_study_event', msg, uri))

        if not r.etype:
            msg = 'study_event without study_type'
            logging.debug(msg + ': %s', uri)
            warnings.append(('check_study_event', msg, uri))

        if not r.witness:
            msg = 'study_event without witness'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_study_event', msg, uri))

        if not r.person:
            msg = 'study_event without person'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_study_event', msg, uri))

    return errors, warnings

def check_misattribution(store, errors, warnings):
    """check ismi:Misattribution
    * has crm class
    * has ismi_id
    * has text, person
    """
    query = '''SELECT DISTINCT ?event ?crm ?ismi_id ?text ?person
        WHERE {
          ?event a ismi:Misattribution .
          OPTIONAL {
            ?event a ?crm .
            filter(?crm = crm:E13_Attribute_Assignment)
          }
          OPTIONAL {
            ?event crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?text frbroo:R16i_was_initiated_by / crm:P140i_was_attributed_by ?event .
          }
          OPTIONAL {
            ?event crm:P141_assigned ?person .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?text) || !bound(?person))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.event
        if not r.crm:
            msg = 'misattribution without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misattribution', msg, uri))

        if not r.ismi_id:
            msg = 'misattribution without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misattribution', msg, uri))

        if not r.text:
            msg = 'misattribution without text'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misattribution', msg, uri))

        if not r.person:
            msg = 'misattribution without person'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misattribution', msg, uri))

    return errors, warnings

def check_misidentification(store, errors, warnings):
    """check ismi:Misidentification
    * has crm class
    * has ismi_id
    * has witness, person
    """
    query = '''SELECT DISTINCT ?event ?crm ?ismi_id ?witness ?person
        WHERE {
          ?event a ismi:Misidentification .
          OPTIONAL {
            ?event a ?crm .
            filter(?crm = crm:E13_Attribute_Assignment)
          }
          OPTIONAL {
            ?event crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?witness crm:P140i_was_attributed_by ?event .
          }
          OPTIONAL {
            ?event crm:P141_assigned ?exp .
            ?exp a frbroo:F22_Self-Contained_Expression ;
              frbroo:R9i_realises ?text .
            ?text a frbroo:F14_Individual_Work ;
              frbroo:R16i_was_initiated_by ?creat .
            ?creat a frbroo:F27_Work_Conception ;
              crm:P14_carried_out_by ?person .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?witness) || !bound(?person))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.event
        if not r.crm:
            msg = 'misidentification without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misidentification', msg, uri))

        if not r.ismi_id:
            msg = 'misidentification without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misidentification', msg, uri))

        if not r.witness:
            msg = 'misidentification without witness'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misidentification', msg, uri))

        if not r.person:
            msg = 'misidentification without person'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_misidentification', msg, uri))

    return errors, warnings

def check_reference(store, errors, warnings):
    """check ismi:Reference
    * has crm class
    * has ismi_id
    * has bib item, ismi item
    """
    query = '''SELECT DISTINCT ?reference ?crm ?ismi_id ?bib ?item ?label
        WHERE {
          ?reference a ismi:Reference .
          OPTIONAL {
            ?reference a ?crm .
            filter(?crm = crm:E73_Information_Object)
          }
          OPTIONAL {
            ?reference crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?reference crm:P67_refers_to ?bib .
          }
          OPTIONAL {
            ?item crm:P129i_is_subject_of ?reference .
          }
          OPTIONAL {
            ?reference rdfs:label ?label .
            FILTER(contains(?label, "#"))
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?bib) || !bound(?item) || !bound(?label))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.reference
        if not r.crm:
            msg = 'reference without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_reference', msg, uri))

        if not r.ismi_id:
            msg = 'reference without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_reference', msg, uri))

        if not r.bib:
            msg = 'reference without bib item'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_reference', msg, uri))

        if not r.item:
            msg = 'reference without ismi item'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_reference', msg, uri))

        if not r.label:
            msg = 'reference without bib_id in label'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_reference', msg, uri))

    return errors, warnings

def check_floruit(store, errors, warnings):
    """check ismi:Floruit
    * has crm class
    * has ismi_id
    * has date, item
    """
    query = '''SELECT DISTINCT ?floruit ?crm ?ismi_id ?date ?item
        WHERE {
          ?floruit a ismi:Floruit .
          OPTIONAL {
            ?floruit a ?crm .
            filter(?crm = frbroo:F51_Pursuit)
          }
          OPTIONAL {
            ?floruit crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?floruit crm:P4_has_time-span ?date .
          }
          OPTIONAL {
            ?item crm:P14i_performed ?floruit .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?date) || !bound(?item))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.floruit
        if not r.crm:
            msg = 'floruit without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_floruit', msg, uri))

        if not r.ismi_id:
            msg = 'floruit without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_floruit', msg, uri))

        if not r.date:
            msg = 'floruit without date'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_floruit', msg, uri))

        if not r.item:
            msg = 'reference without ismi item'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_reference', msg, uri))

    return errors, warnings

def check_digitization(store, errors, warnings):
    """check ismi:Digitization
    * has crm class
    * has ismi_id
    * has manifest
    """
    query = '''SELECT DISTINCT ?digitization ?crm ?ismi_id ?manif
        WHERE {
          ?digitization a ismi:Digitization .
          OPTIONAL {
            ?digitization a ?crm .
            filter(?crm = crmdig:D2_Digitization_Process)
          }
          OPTIONAL {
            ?digitization crm:P1_is_identified_by ?id .
            ?id a crm:E42_Identifier ;
              crm:P2_has_type ismiidtype:ismi-id ;
              rdfs:label ?ismi_id .
          }
          OPTIONAL {
            ?digitization crm:P94_has_created ?manif .
            ?manif a crm:E36_Visual_Item ;
              a crmdig:D1_Digital_Object ;
              crm:P2_has_type <https://iiif.io/api/presentation/#> .
          }
          FILTER (!bound(?crm) || !bound(?ismi_id) || !bound(?manif))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.digitization
        if not r.crm:
            msg = 'digitization without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_digitization', msg, uri))

        if not r.ismi_id:
            msg = 'digitization without ismi_id'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_digitization', msg, uri))

        if not r.manif:
            msg = 'digitization without manifest'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_digitization', msg, uri))

    return errors, warnings

def check_alias(store, errors, warnings):
    """check ismi:Alias
    * has crm class
    * has ismi_id
    * has witness, person
    """
    query = '''SELECT DISTINCT ?alias ?crm ?atype ?item
        WHERE {
          ?alias a ismi:Alias .
          OPTIONAL {
            ?alias a ?crm .
            filter(?crm = crm:E42_Identifier || ?crm = crm:E41_Appellation || ?crm = crm:E35_Title || ?crm = frbroo:F23_Expression_Fragment)
          }
          OPTIONAL {
            ?alias crm:P2_has_type ?atype .
          }
          OPTIONAL {
            ?item crm:P1_is_identified_by | crm:P102_has_title | frbroo:R15_has_fragment ?alias .
          }
          FILTER (!bound(?crm) || !bound(?atype) || !bound(?item))
        }'''
    res = store.query(query)
    for r in res:
        uri = r.alias
        if not r.crm:
            msg = 'alias without crm class'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_alias', msg, uri))

        if not r.atype:
            msg = 'alias without alias type'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_alias', msg, uri))

        if not r.item:
            msg = 'alias without ismi item'
            logging.debug(msg + ': %s', uri)
            errors.append(('check_alias', msg, uri))

    return errors, warnings


## 
## main
##
argp = argparse.ArgumentParser(description='Validate ISMI-CIDOC RDF in triple store using SPARQL queries.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('sparql_url', nargs='?', help='triplestore SPARQL endpoint URL',
                  default='http://localhost:8082/blazegraph/namespace/kb/sparql')
argp.add_argument('-d', '--show-details', dest='details',
                  help='Show error details for specified objects ("person", "text", etc. comma separated)')
argp.add_argument('--detail-level', dest='detail_level', choices=['WARNING', 'ERROR', 'ALL'], default='ALL', 
                  help='Level of error details ("WARNING", "ERROR", "ALL")')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
argp.add_argument('--log-to', dest='log_to', default='STDERR', 
                  help='Log destination (STDOUT, STDERR).')
args = argp.parse_args()

if args.log_to == 'STDOUT':
    logstream = sys.stdout
else:
    logstream = sys.stderr
    
logging.basicConfig(level=args.loglevel, stream=logstream)

# types of checks
check_types = ['class', 'ismi_id', 'person', 'text', 'witness', 'codex', 'collection', 'repository', 
               'place', 'role', 'subject', 
               'transfer_event', 'study_event',
               'misattribution', 'misidentification', 
               'reference', 'floruit', 'digitization', 'alias']

detail_types = set()
if args.details:
    for t in args.details.split(','):
        if t.strip() not in check_types:
            logging.error(f"Invalid detail type: {t}!")
            sys.exit(1)
        else:
            detail_types.add(t.strip())

# connect to triplestore
store = ConjunctiveGraph('SPARQLStore')
logging.info(f"connecting to SPARQL endpoint {args.sparql_url}")
store.open(args.sparql_url)
for pref in nsPrefixes:
    store.bind(pref, nsPrefixes[pref])

# run tests
errors = {}
warnings = {}
all_errors = []
all_warnings = []

for check_type in check_types:
    logging.info(f"  checking {check_type}")
    errors[check_type] = []
    warnings[check_type] = []
    locals()[f"check_{check_type}"](store, errors[check_type], warnings[check_type])
    all_errors += errors[check_type]
    all_warnings += warnings[check_type]

logging.info(f"Found {len(all_errors)} errors and {len(all_warnings)} warnings.")
# count messages
msgs = {}
for check, msg, uri in all_errors:
    if msg in msgs:
        msgs[msg] += 1
    else:
        msgs[msg] = 1

for check, msg, uri in all_warnings:
    if msg in msgs:
        msgs[msg] += 1
    else:
        msgs[msg] = 1

# print counted messages
for msg, cnt in msgs.items():
    logging.info(f"  {msg}: {cnt}")

# print details
for dt in detail_types:
    if args.detail_level == 'ERROR' or args.detail_level == 'ALL':
        logging.info(f"Errors for {dt}: {errors[dt]}")

    if args.detail_level == 'WARNING' or args.detail_level == 'ALL':
        logging.info(f"Warnings for {dt}: {warnings[dt]}")
