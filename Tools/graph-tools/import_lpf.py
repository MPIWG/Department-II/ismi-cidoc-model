from rdflib import Graph, Literal, URIRef, ConjunctiveGraph
from rdflib.namespace import Namespace
from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore
from shapely import from_geojson, GeometryCollection, MultiPoint, Point
import uuid
import json
import logging
import re
import sys
import argparse

__version__ = '1.3'

##
## namespaces
##
rdfNs = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
rdfsNs = Namespace('http://www.w3.org/2000/01/rdf-schema#')
crmNs = Namespace('http://www.cidoc-crm.org/cidoc-crm/')
digNs = Namespace('http://www.ics.forth.gr/isl/CRMdig/')
frbrNs = Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/')
skosNs = Namespace('http://www.w3.org/2004/02/skos/core#')
ismiNs = Namespace('http://ontologies.mpiwg-berlin.mpg.de/ismi/')
ismiIdTypeNs = Namespace('http://content.mpiwg-berlin.mpg.de/ns/ismi/type/identifier/')
nsPrefixes = {'crm': crmNs, 'dig': digNs, 'frbroo': frbrNs, 'ismi': ismiNs, 'ismiidtype': ismiIdTypeNs}

##
## patterns
##
ismi_object_id_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/([-\w]+)/([-\w]+)')
ismi_type_pattern = re.compile(r'http://content.mpiwg-berlin.mpg.de/ns/ismi/type/([-\w]+)/([-\w]+)')
ismi_web_place_id_pattern = re.compile(r'http://ismi.mpiwg-berlin.mpg.de/place/(\w+)')

##
## URLs
##
ismi_content_base_uri = 'http://content.mpiwg-berlin.mpg.de/ns/ismi'
ismi_graph_postfix = '/container/context'
external_graph_uri = ismi_content_base_uri + '/external/container/context'
linked_place_uri = ismi_content_base_uri + '/linked-place/%s'
linked_place_graph_uri = ismi_content_base_uri + '/linked-place/container/context'
linked_place_type_uri = ismi_content_base_uri + '/type/place/linked-place'
place_pref_name_type_uri = ismi_content_base_uri + '/type/name/preferred-name'
place_alias_name_type_uri = ismi_content_base_uri + '/type/name/alias-name'


def get_ismi_places(store):
    """Return a dict of ismi:Place ids and uris."""
    places = {}
    query = '''SELECT DISTINCT ?uri ?ismi_id ?label
        WHERE {
          ?uri a ismi:Place ;
            rdfs:label ?label ;
            crm:P1_is_identified_by ?id .
          ?id crm:P2_has_type ismiidtype:ismi-id ;
            rdfs:label ?ismi_id .
        }'''
    res = store.query(query)
    for r in res:
        ismi_id = str(r.ismi_id)
        places[ismi_id] = {'uri': r.uri, 'label': str(r.label)}
    
    logging.info(f"  found {len(places)} places in ismi db")
    return places


def get_place_names(store, place_uri):
    """Return a dict of place names and name_uris."""
    names = {}
    query = '''SELECT DISTINCT ?name_uri ?label ?n_type
        WHERE {
          ?place_uri a ismi:Place ;
            crm:P1_is_identified_by ?name_uri .
          ?name_uri a crm:E41_Appellation ;
            crm:P2_has_type ?n_type ;
            rdfs:label ?label .
        }'''
    res = store.query(query, initBindings={'place_uri': place_uri})
    for r in res:
        name_uri = r.name_uri
        names[name_uri] = {'type': r.n_type, 'label': str(r.label)}
    
    return names


def load_lpf_file(infile):
    """Load LPF-JSON file and return dict sorted by ismi:Place."""
    logging.info(f"  loading {infile}")
    with open(infile, 'r') as fh:
        lpf_json = json.load(fh)
        
    if not lpf_json:
        sys.exit(f"Unable to load LPF-JSON file {infile}!")
        
    ismi_place_info = {}
    geom_cnt = 0
    name_cnt = 0
    for feature in lpf_json['features']:
        prop = feature['properties']
        ismi_id = prop['src_id']
        if ismi_id.startswith('ismi:'):
            ismi_id = ismi_id[5:]
            
        place_info = {
            'whg_id': prop['pid'],
            'title': prop['title']
        }
        
        # read geometry
        geometry = None
        shape = from_geojson(json.dumps(feature['geometry']))
        if type(shape) == GeometryCollection:
            # extract single shape
            coll = shape
            shape = None
            prev_s = None
            for s in coll.geoms:
                # ignore duplicate geometries
                if not s.equals_exact(prev_s, 0.001):
                    if shape is not None:
                        logging.warning(f"Ignoring more than one geometry in {feature['geometry']}!")
                        continue
                        
                    shape = s
                    prev_s = s
                
        if type(shape) == MultiPoint:
            if len(shape.geoms) == 1:
                # extract single point
                shape = shape.geoms[0]
            
        elif shape is not None:
            logging.warning(f"Unknown geometry type {shape}")
            
        if shape:
            # use WKT
            geometry = shape.wkt
            geom_cnt += 1
            
        # read names
        toponyms = {}
        for name in feature['names']:
            nid = name['citations'][0]['id']
            # ingore ismi toponyms
            if nid == "" or nid.startswith('http://ismi.mpiwg-berlin.mpg.de/place'):
                if nid == "":
                    # sometimes id is empty
                    nid = name['citations'][0]['label']
                    
                m = ismi_web_place_id_pattern.match(nid)
                if m:
                    nid = 'ismi:' + m.group(1)
                    
                else:
                    logging.warning(f"Unknown place id: {nid}")
                    continue
            
            if nid in toponyms:
                toponyms[nid].append(name['toponym'])
            else:
                toponyms[nid] = [name['toponym']]
                name_cnt += 1

        # read links
        links = set()
        for link in feature['links']:
            if link['type'] == 'closeMatch':
                ext_id = link['identifier']
                links.add(ext_id)
            else:
                logging.warning(f"Unknown link type {link['type']}!")
                
        # save
        if geometry:
            place_info['geometry'] = geometry
        
        if toponyms:
            place_info['toponyms'] = toponyms

        if links:
            place_info['links'] = links
            
        logging.debug(f"place_info[{ismi_id}] = {place_info}")
        ismi_place_info[ismi_id] = place_info
    
    logging.info(f"  loaded {geom_cnt} geometries and {name_cnt} names for {len(ismi_place_info)} places")
    return ismi_place_info


def add_info_to_place(store, add_info, ismi_uri):
    """Add external information in add_info to ismi:Place."""
    new_quads = []
    graph = store.get_context(linked_place_graph_uri)
    if 'links' in add_info:
        for link in add_info['links']:
            link_uri = URIRef(linked_place_uri%link)
            new_quads.append((ismi_uri, skosNs.closeMatch, link_uri, graph))
            new_quads.append((link_uri, rdfNs.type, crmNs.E53_Place, graph))
            new_quads.append((link_uri, crmNs.P2_has_type, URIRef(linked_place_type_uri), graph))
            new_quads.append((link_uri, rdfsNs.label, Literal(str(link)), graph))
            new_quads.append((link_uri, rdfNs.type, crmNs.E53_Place, graph))
        
    if 'geometry' in add_info:
        geo_primitive = Literal(add_info['geometry'])
        new_quads.append((ismi_uri, crmNs.P168_place_is_defined_by, geo_primitive, graph))

    if new_quads:
        store.addN(new_quads)
        logging.debug(f"added {len(new_quads)} triples for place {ismi_uri}")


def import_lpf(store, args):
    """Import LPF file with additional place data and add to existing ismi:Places.
    """
    ismi_place_add_info = load_lpf_file(args.infile)
    ismi_places = get_ismi_places(store)
    
    if ismi_place_add_info and ismi_places:
        # remove all triples from bibliography graph
        logging.info("  removing old linked-places graph")
        graph = store.get_context(linked_place_graph_uri)
        store.remove_context(graph)

    logging.info("  adding data to linked-places graph")
    add_cnt = 0
    for ismi_id in ismi_places:
        ismi_uri = ismi_places[ismi_id]['uri']
        if str(ismi_id) in ismi_place_add_info:
            add_info = ismi_place_add_info[str(ismi_id)]
            if 'geometry' in add_info or 'links' in add_info:
                add_cnt += 1
                add_info_to_place(store, add_info, ismi_uri)
                
        else:
            logging.debug(f" no add_info for {ismi_id=}")
            
    logging.info(f"added external information for {add_cnt} ismi places out of {len(ismi_places)}.")
    
    
def change_place_name(store, place_uri, new_name):
    """Change preferred name of ismi:Place.
    
       Keeps the old name as alias.
    """
    ismi_names = get_place_names(store, place_uri)
    old_alias_uri = None
    old_pref_name = None
    old_pref_name_uri = None
    # check existing appellations
    for name_uri in ismi_names:
        if str(ismi_names[name_uri]['type']) == place_pref_name_type_uri:
            old_pref_name_uri = name_uri
            old_pref_name = ismi_names[name_uri]['label']
            
        if new_name == ismi_names[name_uri]['label']:
            if str(ismi_names[name_uri]['type']) == place_pref_name_type_uri:
                raise RuntimeError(f"New preferred name {new_name} exists as preferred name {name_uri}!")
            
            elif str(ismi_names[name_uri]['type']) == place_alias_name_type_uri:
                logging.debug(f"new preferred name {new_name} exists as alias name {name_uri}")
                old_alias_uri = name_uri
                
    if not old_pref_name and old_pref_name_uri:
        raise RuntimeError(f"Missing preferred name for place {place_uri}!")
    
    new_quads = []
    graph = store.get_context(URIRef(str(place_uri) + ismi_graph_postfix))
    # change place label
    store.remove(((place_uri, rdfsNs.label, Literal(old_pref_name), graph)))
    new_quads.append((place_uri, rdfsNs.label, Literal(new_name), graph))
    # change name label in preferred name
    store.remove((old_pref_name_uri, rdfsNs.label, Literal(old_pref_name), graph))
    new_quads.append((old_pref_name_uri, rdfsNs.label, Literal(new_name), graph))
    # add old name as new alias
    new_alias_uri = URIRef(str(place_uri) + '/name/' + str(uuid.uuid4()))
    new_quads.append((place_uri, crmNs.P1_is_identified_by, new_alias_uri, graph))
    new_quads.append((new_alias_uri, rdfNs.type, crmNs.E41_Appellation, graph))
    new_quads.append((new_alias_uri, rdfNs.type, ismiNs.Alias, graph))
    new_quads.append((new_alias_uri, crmNs.P2_has_type, URIRef(place_alias_name_type_uri), graph))
    new_quads.append((new_alias_uri, rdfsNs.label, Literal(old_pref_name), graph))
    # save triples
    store.addN(new_quads)


def fix_place_names(store, args):
    """Import LPF file with place data and correct existing ismi:Place names.
    """
    ismi_place_add_info = load_lpf_file(args.infile)
    ismi_places = get_ismi_places(store)
    
    logging.info("  correcting ismi_place names")
    chg_cnt = 0
    for ismi_id in ismi_places:
        ismi_uri = ismi_places[ismi_id]['uri']
        place_label = ismi_places[ismi_id]['label']
        if str(ismi_id) in ismi_place_add_info:
            add_info = ismi_place_add_info[str(ismi_id)]
            title = add_info['title']
            if place_label != title:
                logging.debug(f"change place[{ismi_id}] label {repr(place_label)} to {repr(title)}")
                chg_cnt += 1
                change_place_name(store, ismi_uri, title)
            
    logging.info(f"corrected place names for {chg_cnt} ismi places out of {len(ismi_places)}.")
    
    
## 
## main
##
argp = argparse.ArgumentParser(description='Import Linked-Place-Format (LPF) JSON into ISMI db.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('infile', help='Input file in LPF JSON format')
argp.add_argument('--fix-place-names', dest='fix_place_names', action='store_true', 
                  help='Correct place names in ISMI db based on this list.')
argp.add_argument('-u', '--url', dest='sparql_url', help='triplestore SPARQL endpoint URL',
                  default='http://localhost:8082/blazegraph/namespace/kb/sparql')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
argp.add_argument('--log-to', dest='log_to', default='STDERR', 
                  help='Log destination (STDOUT, STDERR).')
args = argp.parse_args()

if args.log_to == 'STDOUT':
    stream = sys.stdout
else:
    stream = sys.stderr
    
logging.basicConfig(level=args.loglevel, stream=stream)

# triplestore address
sparql_endpoint = args.sparql_url

## connect to triplestore
store = ConjunctiveGraph('SPARQLUpdateStore')
logging.info(f"connecting to SPARQL endpoint {sparql_endpoint}")
store.open((sparql_endpoint, sparql_endpoint))
for pref in nsPrefixes:
    store.bind(pref, nsPrefixes[pref])

if args.fix_place_names:
    logging.info("correcting ISMI place names")
    fix_place_names(store, args)
else:
    logging.info("importing Linked Places Format data...")
    import_lpf(store, args)
