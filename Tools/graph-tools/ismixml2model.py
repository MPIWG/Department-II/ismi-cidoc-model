import xml.etree.ElementTree as ET
import json
import networkx
import sys
import logging
import argparse
import pickle

## configure behaviour

# process only public objects
public_only = False

# node types to exclude from the graph
#exclude_objects_of_type = ['DIGITALIZATION', 'REFERENCE']
exclude_objects_of_type = []

# attributes to exclude
exclude_attributes_of_type = [
    'lw',
    'node_type',
    'nov',
#    'notes_old'
]

# name of type attribute
node_type_attribute = '_type'
rel_type_attribute = '_type'

#ismi_types=["PERSON","WITNESS","CODEX","PLACE","COLLECTION","REPOSITORY"]


nx_graph = networkx.MultiDiGraph()

nx_nodes = {}
ismi_relations = {}
nx_relations = {}

def fixName(name, is_src_rel=False, is_tar_rel=False, att_from_rel=False):
    if is_src_rel:
        #name = name + '>'
        pass
        
    if is_tar_rel:
        name = '<' + name
        
    if att_from_rel:
        # clean up relations as attribute names
        name = name.replace('is_', '')
        name = name.replace('has_', '')
        name = name.replace('was_', '')
        name = name.replace('_of', '')

    return name



def parseYear(val):
    year = None
    try:
        date_json = json.loads(val)
        if 'from' in date_json:
            year = date_json['from'].get('year', None)
        elif 'date' in date_json:
            year = date_json['date'].get('year', None)
        else:
            logging.debug("don't know what to do with date %s"%(val))
            
    except:
        pass
    
    return year

def parseIsmiDate(elem):
    date_elem = elem.find('ismi-date')
    if date_elem is not None:
        type = date_elem.get('type')
        calendar = date_elem.get('calendar')
        label = date_elem.text
        notes = date_elem.get('notes')
        if type == 'unspecified':
            return {
                'type': type,
                'label': label,
            }
        elif type == 'day':
            date = date_elem.get('date')
            year = date[:4]
            return {
                'type': type,
                'calendar': calendar,
                'label': label,
                'date': date,
                'year': year,
                'notes': notes
            }
        elif type == 'year' or type == 'range':
            date = date_elem.get('from')
            year = date[:4]
            return {
                'type': type,
                'calendar': calendar,
                'label': label,
                'from': date_elem.get('from'),
                'until': date_elem.get('until'),
                'year': year,
                'notes': notes
            }
        else:
            logging.error(f"Invalid date type={type}")            

    return None

def nodeFromEnt(ent_elem):
    """Create a graph node from the given XML entity.
    
    Creates the node in gdb and returns the node.
    """     
    # text content of entity element
    ov = ent_elem.text or ''
    # add text remainders from child elements
    for elem in ent_elem:
        if elem.tail is not None:
            ov += elem.tail

    attrs = {}

    # get attributes element
    atts_elem = ent_elem.find('attributes')

    if atts_elem is None:
        logging.debug("entity has no attributes: %s"%ent_elem)
        
    else:
        # go through all attributes
        for att_elem in atts_elem:
            ct = att_elem.get('content-type', None)
            name = att_elem.get('name', None)
            if name in exclude_attributes_of_type:
                # exclude attribute
                continue
    
            if ct is None or ct.lower() in ['text', 'arabic', 'bool', 'boolean', 'url', 'language', 'escidoc-objid', 'null']:
                # normal text attribute (assume no content_type is text too...)
                val = att_elem.text
                
                if val is not None and val[0] == '{':
                    # try to parse as date
                    date = parseIsmiDate(att_elem)
                    if date is not None:
                        for part in ['label', 'type', 'calendar', 'notes', 'from', 'until', 'date']:
                            if part in date and date[part]:
                                # create attributes for date parts
                                attrs[name+'_'+part] = date[part]
                        
                        if 'year' in date and date['year']:
                            # save year in attribute
                            attrs[name] = date['year']
                            
                        continue
                    
                if val is not None:
                    # keep attribute
                    attrs[name] = val
                    # check for normalized value
                    nov = att_elem.findtext('norm')
                    if nov is not None:
                        # add normalized value
                        attrs['_n_'+name] = nov
                
            elif ct == 'date':
                # date attribute
                date = parseIsmiDate(att_elem)
                if date is not None:
                    for part in ['label', 'type', 'calendar', 'notes', 'from', 'until', 'date']:
                        if part in date and date[part]:
                            # create attributes for date parts
                            attrs[name+'_'+part] = date[part]
                    
                    if 'year' in date and date['year']:
                        # save year in attribute
                        attrs[name] = date['year']
                
            elif ct == 'num':
                # number attribute
                val = att_elem.text
                if val is not None:
                    # keep attribute, assume num is int
                    attrs[name] = int(val)
                
            elif ct == 'old':
                # ignore attribute
                continue
                
            else:
                logging.warning("attribute with unknown content_type: %s"%ct)
                # ignore other content types
                continue
            
    #
    # process base attributes
    #
    # type
    oc = ent_elem.get('object-class')
    attrs[node_type_attribute] = fixName(oc)

    # id
    ismi_id = int(ent_elem.get('id'))
    # rename id to ismi_id
    attrs['ismi_id'] = ismi_id
    
    # public
    pub = ent_elem.get('public')
    attrs['_public'] = (pub == 'true')
            
    if len(ov) > 0:
        # save ov as label
        attrs['_label'] = ov
        # check for normalized value
        nov = ent_elem.findtext('norm')
        if nov is not None:
            # add normalized value
            attrs['_n_label'] = nov
    
    # create node
    logging.debug("new node(%s, %s)"%(ismi_id, attrs))
    nx_graph.add_node(ismi_id, **attrs)
    node = nx_graph.nodes[ismi_id]
    
    return node


def relationFromRel(rel_elem):
    """Create graph relation from etree element.    
    """
    rel_id = int(rel_elem.get('id'))
    rel_name = rel_elem.get('object-class')
    src_id = int(rel_elem.get('source-id'))
    tar_id = int(rel_elem.get('target-id'))
    if not src_id in nx_nodes:
        if public_only:
            logging.debug("relation %s src node %s missing!"%(rel_id,src_id))
        else:
            logging.warning("relation %s src node %s missing!"%(rel_id,src_id))
            
        return None
    
    if not tar_id in nx_nodes:
        if public_only:
            logging.debug("relation %s tar node %s missing!"%(rel_id,tar_id))
        else:
            logging.warning("relation %s tar node %s missing!"%(rel_id,tar_id))
            
        return None

    ov = rel_elem.text or ''

    attrs = {}

    # get attributes element
    atts_elem = rel_elem.find('attributes')
    
    if atts_elem is not None:
        if atts_elem.tail is not None:
            # tail belongs to parent
            ov += atts_elem.tail
        
        # go through all attributes
        for att_elem in atts_elem:
            ct = att_elem.get('content-type', None)
            name = att_elem.get('name', None)
            if name in exclude_attributes_of_type:
                # exclude attribute
                continue
    
            if ct is None or ct.lower() in ['text', 'arabic', 'bool', 'boolean', 'url', 'language', 'null']:
                # normal text attribute (assume no content_type is text too...)
                val = att_elem.text
                
                if val is not None and val[0] == '{':
                    # try to parse as date
                    date = parseIsmiDate(att_elem)
                    if date is not None:
                        for part in ['label', 'type', 'calendar', 'notes', 'from', 'until', 'date']:
                            if part in date and date[part]:
                                # create attributes for date parts
                                attrs[name+'_'+part] = date[part]
                        
                        if 'year' in date and date['year']:
                            # save year in attribute
                            attrs[name] = date['year']
                            
                        continue
                    
                if val is not None:
                    # keep attribute
                    attrs[name] = val
                     # check for normalized value
                    nov = att_elem.findtext('norm')
                    if nov is not None:
                        # add normalized value
                        attrs['_n_'+name] = nov
                
            elif ct == 'date':
                # date attribute
                date = parseIsmiDate(att_elem)
                if date is not None:
                    for part in ['label', 'type', 'calendar', 'notes', 'from', 'until', 'date']:
                        if part in date and date[part]:
                            # create attributes for date parts
                            attrs[name+'_'+part] = date[part]
                    
                    if 'year' in date and date['year']:
                        # save year in attribute
                        attrs[name] = date['year']
                
            elif ct == 'num':
                # number attribute
                val = att_elem.text
                if val is not None:
                    # keep attribute, assume num is int
                    attrs[name] = int(val)
                
            elif ct == 'old':
                # ignore attribute
                continue
                
            else:
                logging.warning("attribute with unknown content_type: %s"%repr(att_elem))
                # ignore other content types
                continue
    
    #if len(ov) > 0:
    #    # own value of relation is not useful
    #    attrs['ov'] = ov

    # set type        
    attrs[rel_type_attribute] = fixName(rel_name)
    # set id
    attrs['ismi_id'] = rel_id
    # set public
    pub = rel_elem.get('public')
    attrs['_public'] = (pub == 'true')
            
    # create relation with type
    nx_rel = nx_graph.add_edge(src_id, tar_id, attr_dict=attrs)
    
    return nx_rel


def importEnts(ents_elem):
    """Import all entities from etree element elem.
    """
    cnt = 0
    xml_num = ents_elem.get('count')
    logging.info("XML says %s entities"%xml_num)
    
    # iterate through entities element
    for ent_elem in ents_elem:
        cnt += 1
        
        oc = ent_elem.get('object-class')
        if oc in exclude_objects_of_type:
            # skip this entity
            continue
        
        if public_only and (ent_elem.get('public', None) != 'true'):
            # skip non-public
            continue
        
        ismi_id = int(ent_elem.get('id'))
        logging.debug("reading entity[%s]"%ismi_id)
        
        if ismi_id in nx_nodes:
            logging.error("entity with id=%s exists! Aborting."%ismi_id)
            sys.exit(1)
        
        # create networkx node
        node = nodeFromEnt(ent_elem)
        
        # save node reference
        nx_nodes[ismi_id] = node
        
        # debug        
        #if cnt >= 100:
        #    return


def importRels(rels_elem):
    """Import all entities from etree element elem.
    """
    cnt = 0
    xml_num = rels_elem.get('count')
    logging.info("XML says %s relations"%xml_num)
    
    # iterate through entities element
    for rel_elem in rels_elem:
        cnt += 1

        ismi_id = int(rel_elem.get('id'))
        logging.debug("reading relation[%s]"%ismi_id)
        
        if ismi_id in nx_relations:
            logging.error("relation with id=%s exists! Aborting."%ismi_id)
            sys.exit(1)
        
        # create networkx relation
        relation = relationFromRel(rel_elem)
        
        # save relation reference
        nx_relations[ismi_id] = relation
        
        # debug
        #if cnt >= 100:
        #    return


def importAll(input_fn):
    # parse XML file
    logging.info("parsing XML file %s"%input_fn)
    tree = ET.parse(input_fn)
    logging.debug("etree ready")
    root = tree.getroot()
    ents = root.find('entities')
    importEnts(ents)
        
    rels = root.find('relations')
    importRels(rels)

## main
argp = argparse.ArgumentParser(description='Copy graph from OpenMind-XML to networkx pickle.')
argp.add_argument('infile', help='Input graph in OpenMind-XML format',
                  default='openmind-data.xml')
argp.add_argument('outfile', help='Output graph in gpickle format',
                  default='ismi_graph.gpickle')
argp.add_argument('-public', '--public-only', dest='public_only', action='store_true', 
                  help='Copy only public entities.')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

logging.basicConfig(level=args.loglevel)

public_only = args.public_only

# import everything
logging.info("Reading graph from OpenMind-XML file %s"%args.infile)
if len(exclude_objects_of_type) > 0:
    logging.info("  Skipping objects of type %s"%exclude_objects_of_type);
    
if public_only:
    logging.info("  Skipping all non-public objects.")
    
importAll(args.infile)

logging.info("Graph info: %s"%(nx_graph))
#logging.info(" nodes:%s"%repr(nx_graph.nodes(data=True)))

# export pickle
with open(args.outfile, 'wb') as f:
    pickle.dump(nx_graph, f, pickle.HIGHEST_PROTOCOL)
    logging.info("Wrote networkx pickle file %s"%args.outfile)
