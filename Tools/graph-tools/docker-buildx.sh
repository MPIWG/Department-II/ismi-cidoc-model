#!/bin/sh
IMAGE='docker.gitlab.gwdg.de/mpiwg/department-ii/ismi-cidoc-model/graph-tools:latest'
docker buildx build --platform=linux/amd64,linux/arm64 --push -t $IMAGE --provenance false .
