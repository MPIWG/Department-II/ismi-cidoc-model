import xml.etree.ElementTree as ET
import networkx
import logging
import sys
import argparse
import pickle

__version__ = '1.5'

ismi_xml_version = '5.1'

## configure behaviour

# node types to exclude from the graph
#exclude_objects_of_type = ['DIGITALIZATION', 'REFERENCE']
exclude_objects_of_type = []

# attributes to ignore
exclude_attributes = {'hijra_date', 'num_death_date', 'death_date_text', 'birth_date_text', 'creation_date_num'}

# set everything to public
set_all_public = False

# name of type attribute
node_type_attribute = '_type'
rel_type_attribute = '_type'


def elemFromEnt(nx_graph, node_id):
    """Create XML element from networkx node node_id.
       Returns etree element.
    """
    elem = ET.Element('entity')
    dates = {}
    attrs = nx_graph.nodes[node_id]
    att_elems = []
    # process all node attributes
    for att in sorted(attrs.keys()):
        if att in exclude_attributes:
            continue
        
        elif att.startswith('_n_'):
            # ignore old normalized attributes
            continue
        
        val = attrs[att]
        if val is None:
            continue
        
        if att == node_type_attribute:
            elem.set('object-class', val)
            
        elif att == 'ismi_id':
            elem.set('id', str(val))
            
        elif att == '_public':
            elem.set('public', 'true')
            
        elif att == '_label':
            elem.text = val
            
        elif att[0] == '_':
            raise RuntimeError(f"Unknown special node attribute {att}!")
            
        elif att == 'date' or att.endswith('_date') or 'date_' in att:
            # save date elements for processing
            dates[att] = val

        else:
            # create attribute tag
            att_elem = ET.Element('attribute', attrib={'name': att})
            att_elem.text = str(val)
            att_elems.append(att_elem)
            
    if elem.get('id') is None:
        logging.debug(f"setting implicit ismi_id for entity {node_id}")
        elem.set('id', str(node_id))
        
    while dates:
        date_att = sorted(dates.keys(), key=len)[0]
        if date_att.endswith('_type'):
            # maybe unspecified date - create short attribute
            date_att = date_att[:-5]
            dates[date_att] = None
            
        if date_att + '_label' not in dates:
            logging.warning(f"ignoring date without label: {dates}")
            break

        # create attribute tag
        att_elem = ET.Element('attribute', attrib={'name': date_att})
        att_elems.append(att_elem)
        # create ismi-date tag
        date_elem = ET.SubElement(att_elem, 'ismi-date')
        del dates[date_att]
        
        date_elem.text = dates[date_att + '_label']
        att_elem.text = dates[date_att + '_label']
        del dates[date_att + '_label']
        
        date_elem.set('type', dates[date_att + '_type'])
        del dates[date_att + '_type']
        
        if date_att + '_calendar' in dates:
            date_elem.set('calendar', dates[date_att + '_calendar'])
            del dates[date_att + '_calendar']
            
        if date_att + '_date' in dates:
            date_elem.set('date', dates[date_att + '_date'])
            del dates[date_att + '_date']
            
        if date_att + '_from' in dates:
            date_elem.set('from', dates[date_att + '_from'])
            del dates[date_att + '_from']
            
        if date_att + '_until' in dates:
            date_elem.set('until', dates[date_att + '_until'])
            del dates[date_att + '_until']
        
        if date_att + '_notes' in dates:
            date_elem.set('notes', dates[date_att + '_notes'])
            del dates[date_att + '_notes']
    
    if att_elems:
        attrs_elem = ET.SubElement(elem, 'attributes')
        attrs_elem.extend(att_elems)
    
    if set_all_public:
        elem.set('public', 'true')

    return elem
        

def exportEnts(nx_graph, ents_elem):
    """Export all entities in nx_graph to etree element ents_elem.
    """
    cnt = 0
    node_num = nx_graph.number_of_nodes()
    logging.info(f"  found {node_num} nodes in the graph")
    
    # iterate through nodes
    node_ids = sorted(nx_graph.nodes)
    for node_id in node_ids:
        cnt += 1
        attrs = nx_graph.nodes[node_id]
        if not attrs:
            continue
        
        type = attrs[node_type_attribute]
        if exclude_objects_of_type:
            if type in exclude_objects_of_type:
                # skip this entity
                continue
        
        ismi_id = attrs.get('ismi_id')
        logging.debug(f"reading entity[{ismi_id}]")
        
        # create XML element
        elem = elemFromEnt(nx_graph, node_id)
        ents_elem.append(elem)
        
    ents_elem.set('count', str(cnt))
    logging.info(f"  created {cnt} entities in xml")
    return ents_elem


def exportRels(nx_graph, rels_elem):
    """Export all entities in nx_graph to etree element rels_elem.
    """
    cnt = 0
    rels_num = nx_graph.number_of_edges()
    logging.info(f"  found {rels_num} relations in the graph")
    rel_ids = set()
    
    # iterate through relations
    edge_list = sorted(nx_graph.edges)
    for src_id, tar_id, rel_id in edge_list:
        cnt += 1
        # get endpoint ismi_ids and types
        src_ismi_id = nx_graph.nodes[src_id].get('ismi_id', src_id)
        src_type = nx_graph.nodes[src_id].get(node_type_attribute)
        tar_ismi_id = nx_graph.nodes[tar_id].get('ismi_id', tar_id)
        tar_type = nx_graph.nodes[tar_id].get(node_type_attribute)
        # get relation attributes
        attrs = nx_graph.edges[src_id, tar_id, rel_id]['attr_dict']
        rel_type = attrs[rel_type_attribute]
        
        if not (src_ismi_id and src_type and tar_ismi_id and tar_type and rel_type):
            logging.error(f"Invalid relation [({src_type}){src_ismi_id} {rel_type} ({tar_type}){tar_ismi_id}]")
            continue

        logging.debug(f"reading relation[{src_ismi_id} {rel_type} {tar_ismi_id}]")
        
        if exclude_objects_of_type:
            if src_type in exclude_objects_of_type or tar_type in exclude_objects_of_type:
                # skip this entity
                continue
        
        rel_id = f"{src_ismi_id}-{rel_type}-{tar_ismi_id}"
        if rel_id in rel_ids:
            logging.error(f"Duplicate relation id: {rel_id}")
            
        rel_ids.add(rel_id)
        
        # create xml element
        elem = ET.Element('relation', attrib={
            'id': rel_id,
            'object-class': rel_type,
            'source-id': str(src_ismi_id),
            'source-class': src_type,
            'target-id': str(tar_id),
            'target-class': tar_type})
        elem.text = rel_type
        
        rels_elem.append(elem)
        
        # debug
        #if cnt >= 100:
        #    return

    rels_elem.set('count', str(cnt))
    logging.info(f"  created {cnt} relations in xml")
    return rels_elem


def exportAll(nx_graph, output_fn):
    # create XML tree
    root = ET.Element('openmind-data', attrib={'version': ismi_xml_version})
    # add entities
    ents = ET.SubElement(root, 'entities')
    exportEnts(nx_graph, ents)
    # add relations
    rels = ET.SubElement(root, 'relations')
    exportRels(nx_graph, rels)
    # save as file
    tree = ET.ElementTree(root)
    with open(output_fn, 'wb') as f:
        logging.info(f"  writing XML file {output_fn}")
        tree.write(f, encoding='utf-8', xml_declaration=True)

## main
argp = argparse.ArgumentParser(description='Copy graph from networkx pickle to OpenMind-XML.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('infile', help='Input graph in gpickle format',
                  default='ismi_graph.gpickle')
argp.add_argument('outfile', help='Output graph in OpenMind-XML format',
                  default='openmind-data.xml')
argp.add_argument('--set-all-public', dest='set_public', action='store_true', 
                  help='set all exported nodes to public')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
argp.add_argument('--log-to', dest='log_to', default='STDERR', 
                  help='Log destination (STDOUT, STDERR).')
args = argp.parse_args()

if args.log_to == 'STDOUT':
    stream = sys.stdout
else:
    stream = sys.stderr
    
logging.basicConfig(level=args.loglevel, stream=stream)

# import everything
logging.info(f"Reading graph from networkx pickle file {args.infile}")
with open(args.infile, 'rb') as f:
    nx_graph = pickle.load(f)
    logging.info(f"Graph info: {nx_graph}")

if exclude_objects_of_type:
    logging.info(f"  Skipping objects of type {exclude_objects_of_type}");
    
# export XML
exportAll(nx_graph, args.outfile)
logging.info(f"Wrote OpenMind-XML file {args.outfile}")
