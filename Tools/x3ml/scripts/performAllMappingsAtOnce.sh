#!/bin/bash
#set -x
# Perform mapping for resources in a folder

# Arguments
INPUTFOLDER=$1  # A folder containing XML files to map
OUTPUTFOLDER=$2 # A folder for the output files
MAPPINGFOLDER=$3      # The path to the mapping file
GENERATOR=$4    # The generator policy

JAVA_OPTS="-Xmx2048m" # maybe 1g is enough...

if [[ -z "$4"  ]]; then
    echo "Illegal number of parameters"
    exit
fi

numfiles=$(ls -l $INPUTFOLDER/*.xml | wc -l)
count=1
echo "Found $numfiles XML files"

for f in $(ls -1 $INPUTFOLDER/*.xml); do
    echo "Mapping file $count of $numfiles"
    o=${f/.xml/}
    o=${o/$INPUTFOLDER\//}
    # put all mappings in a list
    mappings=$(ls --format=commas $MAPPINGFOLDER/*.*ml | tr -d ' \n')
    echo "Using mappings $mappings"
    java $JAVA_OPTS -jar /x3ml/x3ml-engine.exejar \
      --input $f \
      --x3ml "$mappings" \
      --policy $GENERATOR \
      --output $OUTPUTFOLDER/$o.nt \
      --format application/n-triples
    count=$((count+1))
done
